// filename: Slot.cs
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

/// <summary>
/// This script enables the GameObject it is attached to to receive Drop Events.
/// Attach this script to every GameObject you want to drop other GameObjects onto. 
/// This Script works togather witch DropHandler Scripts.
/// </summary>
public class Slot : MonoBehaviour, IDropHandler, IPointerEnterHandler, IPointerExitHandler
{
    /// <summary>
    /// Used if this slots has silblings.
    /// </summary>
    public int index
    { get; set; }

    public bool highlight = true;

    /// <summary>
    /// Returns the first child of the slot. Which is the thing we are going to drag into another slot.
    /// </summary>
    public GameObject content
    {
        get
        {
            if (transform.childCount > 0)
                return transform.GetChild(0).gameObject;
            return null;
        }
    }

    /// <summary>
    /// Implemntation of the IDropHandlerInterface
    /// </summary>
    /// <param name="eventData"></param>
    public void OnDrop(PointerEventData eventData)
    {
        if (DragHandler.itemBeingDragged)
        {
            Debug.Log("Slot Drop");
            //some fancy color plays.
            GetComponent<Image>().color = Color.white;
            // set teh parent of the item
            DragHandler.itemBeingDragged.transform.SetParent(transform);
            // set the postion of the item.
            DragHandler.itemBeingDragged.transform.position = transform.position;

            // You can also use a Layout on this GameObject then the position is ignored and the Layout script handles the position.
            // But be careful a Layout also overwrite the size of the gameobject.

            // Don't forget this.
            ExecuteEvents.ExecuteHierarchy<IHasChanged>(gameObject, null, (x, y) => x.HasChanged(gameObject));
        }
    }


    // Two Optional Methods you can use. If you dont need them just remove them without worries. 
    // Or use the highlight variable to turn them off.
    // #######################################################################################################################

    /// <summary>
    /// Implemntation of the IPointerEnterHandler
    /// Highlight a slot below the cursor while dragging.
    /// </summary>
    /// <param name="eventData"></param>
    public void OnPointerEnter(PointerEventData eventData)
    {
        if(DragHandler.itemBeingDragged)
        {	
        	if(highlight)
            GetComponent<Image>().color = Color.green;
        }
    }

    /// <summary>
    /// IPointerExitHandler
    /// Reverts the color of the slot after hovering over it while dragging.
    /// </summary>
    /// <param name="eventData"></param>
    public void OnPointerExit(PointerEventData eventData)
    {
        if (DragHandler.itemBeingDragged)
        {
        	if(highlight)
            GetComponent<Image>().color = Color.white;
        }
    }

    // #######################################################################################################################
}