// filename: HasChangedScript.cs
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

/// <summary>
/// Simple template for the implemntation of the IHasChanged Interface.
/// This is usually used to check for if some conditions are met. For example to complete a level.
/// </summary>
public class HasChangedScript : MonoBehaviour, IHasChanged
{

    /// <summary>
    /// Init
    /// </summary>
    private void Start()  {  }

    /// <summary>
    /// IHasChanged implementation.
    /// </summary>
    void IHasChanged.HasChanged(GameObject obj) 
    {
        Debug.Log("New Content added in slot");
    }

}

namespace UnityEngine.EventSystems
{
    public interface IHasChanged : IEventSystemHandler
    {
        void HasChanged(GameObject obj);
    }
}