// filename: DragHandler.cs
using UnityEngine;
using UnityEngine.EventSystems;

/// <summary>
/// Attach this script to every GameObject you want to drag around.
/// </summary>
    public class DragHandler : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler
    {

        // The item that is beeing draged.
        public static GameObject itemBeingDragged;

        // The start position of the item.
        public static Vector3 startPosition;

        // A bool value to decide if the object snaps back after the drag end and no Slot Component was hit.
        public bool snapToStartPosition;

        // the start parent of the item.
        public static Transform startParent;
        public bool Finished { get; set; }

        // Imporant !
        // If you just have imported this package you need to place the "dragfix" prefab first!
        /* Dragfix is basicly a empty RectTransform palced on the bottom of the hierachy of a canvas.
         * And is used to keep the itemBeingDragged on top of everything else.
         * Canvas 
         *   Text
         *   Slot
         *   Slot1
         *   draggable_Item
         *   dragfix
         */
        private Transform dragFix;

        /// <summary>
        /// Init
        /// </summary>
        private void Start()
        {
            dragFix = GameObject.Find("dragfix").transform;
            if(dragFix)
            Debug.Log("DragFix Found");
        }

        /// <summary>
        /// IBeginDragHandler Implementation.
        /// This is called as soon as you begin to drag.
        /// </summary>
        /// <param name="eventData"></param>
        public void OnBeginDrag(PointerEventData eventData)
        {
            // set static variables
            itemBeingDragged = gameObject;
            startPosition = transform.position;
            startParent = transform.parent;

            // set the parent to dragfix
			if(dragFix)
            transform.SetParent(dragFix);
            
            // Don't forget this.
            GetComponent<CanvasGroup>().blocksRaycasts = false;
        }

        /// <summary>
        /// IDragHandler
        /// This is called as long as you drag.
        /// </summary>
        /// <param name="eventData"></param>
        public void OnDrag(PointerEventData eventData)
        {
            // update the position of the item you are dragging.
            transform.position = Input.mousePosition;
        }

        /// <summary>
        /// Implemenation of IEndDragHandler.
        /// This is always called last. Which means after OnDrop and after IHaschanged.
        /// </summary>
        /// <param name="eventData"></param>
        public void OnEndDrag(PointerEventData eventData)
        {
            Debug.Log("Drag End");
            itemBeingDragged = null;

            // Snap reset the parent and position. This will be false if a slot was hit. 
            // Since onDrop would have triggered.
            ////////////////////////////////////////////////////////////////////////
            // Remove this if you want to drag the item everywhere on the screen. //
            ////////////////////////////////////////////////////////////////////////
			if(dragFix)
				if (transform.parent == dragFix.transform)
	            {
	            	if(snapToStartPosition)
	            	{
						transform.position = startPosition;
						transform.SetParent(startParent);
					}
				}


			// Don't forget this.
			GetComponent<CanvasGroup>().blocksRaycasts = true;
		}
	}
