using System;

public static class SessionTimer
{
    private static DateTime Start;

    public static void StartSessionTimer()
    {
        Start = DateTime.Now;
    }

    public static TimeSpan StopSessionTimer()
    {
        return DateTime.Now.Subtract(Start);
    }
}