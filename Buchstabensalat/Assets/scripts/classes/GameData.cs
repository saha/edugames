//file: GameData.cs

// This is an artifact from early builds and is necessary for web builds.
// Web build don't support all Systen.IO methods like Windows. Also the Paths are different.
#if !UNITY_WEBPLAYER
#define SAVE_OK
#endif

using System;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// This is a static class for saving values as static variables globaly.
/// </summary>
public static class GameData
{
    /// <summary>
    /// On android you don't have access to Application.dataPath.
    /// </summary>
    public static string Path
    {
        get
        {
            if (Application.platform == RuntimePlatform.Android)
                return Application.persistentDataPath + System.IO.Path.DirectorySeparatorChar;
            else
                return Application.dataPath + System.IO.Path.DirectorySeparatorChar;
        }
    }

    public static string FirstName = "";
    public static string LastName = "";

    public static int Points = 0;
    public static int Level = 0;

    // This is "special" variable and not used in every game. If you don' track the level progression you can just ignore this.
    // The idea behind this variable is: If you have won X Rounds in succession you get into the next Level.
    public static int LevelProgression = 0;

    public static string Date = "";
    public static int SessionTime = 0;

    /// <summary>
    /// A function to build the path to the save file.
    /// </summary>
    /// <returns>The path to the save file.</returns>
    public static string GetFilePath()
    {
        return Path + GameData.LastName.ToUpper().Trim() + "_" + GameData.FirstName.ToUpper().Trim() + ".txt";
    }

    /// <summary>
    /// Builds the strign that is save in a txt file.
    /// </summary>
    /// <returns></returns>
    public static string GetSaveString()
    {
        return Date + "\t" + Level + "\t" + Points + "\t" + LevelProgression + "\t" + SessionTime + Environment.NewLine;
    }

    /// <summary>
    /// Loads a formated text file.
    /// The default format is:
    /// DATE    LEVEL   POINTS  LEVELPROGRESSION    SESSIONTIME
    /// </summary>
    public static void Load()
    {
#if SAVE_OK
        // TODO: Check for Android built and implementation.
        if (File.Exists(GetFilePath()))
        {
            string line;
            StreamReader reader = new StreamReader(GetFilePath());
            while ((line = reader.ReadLine()) != null)
            {
                if (reader.Peek() == -1)
                    break;
            }
            string[] data = line.Split('\t');

            GameData.Level = Convert.ToInt32(data[1]);
            GameData.Points = Convert.ToInt32(data[2]);
            GameData.LevelProgression = Convert.ToInt32(data[3]);
            Debug.Log("Loaded file: " + GetFilePath() + Environment.NewLine + GetSaveString());
        }
        else
        {
            Points = 0;
            Level = 0;
            Date = "";
            SessionTime = 0;
            LevelProgression = 0;
            Debug.Log("File.Exists(" + GetFilePath() + ") returns false." + Environment.NewLine + "Nothing to load yet. GameState resetted.");
        }
#endif
    }

    /// <summary>
    /// Writes data as ASCI into a txt file.
    /// </summary>
    public static void Save()
    {
#if SAVE_OK
        SessionTime = Convert.ToInt32(SessionTimer.StopSessionTimer().TotalSeconds);
        try
        {
            GameData.Date = DateTime.Now.ToString("dd.MM.yyyy HH:mm");
            File.AppendAllText(GetFilePath(), GetSaveString());
            Debug.Log("Save file: " + GetFilePath() + Environment.NewLine + GetSaveString());
        }
        catch (IOException e)
        {
            UnityEngine.Debug.Log(e.Message + "\n If Overflow- or ConvertException during Save() call, Check SessionTimer.\n And make sure you call SessionTimer.StopSessionTimer() and SessionTimer.StopSessionTimer()");
        }
#endif
    }

    /// <summary>
    /// Used for updating the level.
    /// </summary>
    /// <param name="textField">A label which the level are displayed (optional)</param>
    /// <param name="incrementLevel">If true the level will also incremented by one. If false the level stay the same.</param>
    public static void UpdateLevelLable(Text textField, bool incrementLevel)
    {
        if (incrementLevel)
            GameData.Level++;

        textField.text = "Level:\t" + GameData.Level.ToString();
    }

    /// <summary>
    /// Used for updating the points.
    /// </summary>
    /// <param name="textField">A label which the points are displayed (optional)</param></param>
    /// <param name="incrementPoints">If true the points will also incremented by one. If false the points stay the same.</param>
    public static void UpdatePointLable(Text textField, bool incrementPoints)
    {
        if (incrementPoints)
            GameData.Points++;

        textField.text = "Punkte:\t" + GameData.Points.ToString();
    }
}