// filename: GameStartButtonScript.cs
using UnityEngine;
using System.Collections;

/// <summary>
/// Script for the Start Gamebutton.
/// You might need to adjust the parameter "1" depending on your build settings.
/// </summary>
public class GameStartButtonScript : MonoBehaviour
{

    public void GameStartPressed()
    {
        Application.LoadLevel(1);
    }
}
