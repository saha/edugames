// filename: InputHandler.cs
using UnityEngine;
using System.Collections;

/// <summary>
/// Small script that controlls key inputs for the mainmenu scene.
/// </summary>
public class InputHandler : MonoBehaviour
{

    // Update is called once per frame
    void Update()
    {
        // Call Application.Quit on Escape.
        if (Input.anyKey)
        {
            Application.Quit();
        }
    }
}
