// filename: Utils.cs

using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using UnityEngine;

/// <summary>
/// A static class with some utility functions and static color variabls.
/// </summary>
public static class Utils
{
    // Unity design colors as predefined static variables
    public static Color White = IntegerStringColorToFloatColor("255,255,255,100");
    public static Color UnityBlack = IntegerStringColorToFloatColor("34,44,55,178");
    public static Color UnityRed = HexColorToFloatColor("FF0066");
    public static Color UnityGreen = HexColorToFloatColor("19E3B1");
    public static Color UnityBlue = HexColorToFloatColor("00CCCC");

    // 
    public static Color InkBlue = HexColorToFloatColor("1920AA");

    // TODO: Make a own class for colors. Like Manfred mentaion some sorta pallete with predefinded ColorArrays. 

    
    /// <summary>
    /// A Simple function to shuffle a generic array of type T.
    /// src.: http://www.dotnetperls.com/shuffle
    /// </summary>
    static System.Random _random = new System.Random();
    public static void Shuffle<T>(T[] array)
    {
        int n = array.Length;
        for (int i = 0; i < n; i++)
        {
            // NextDouble returns a random number between 0 and 1.
            // ... It is equivalent to Math.random() in Java.
            int r = i + (int)(_random.NextDouble() * (n - i));
            T t = array[r];
            array[r] = array[i];
            array[i] = t;
        }
    }

    /// <summary>
    /// Converts a hex value into a Unity color which is float.
    /// </summary>
    /// <param name="hex"></param>
    /// <returns></returns>
    public static Color HexColorToFloatColor(string hex)
    {
        Color c = new Color();
        if (hex != null && hex.Length > 0)
        {
            try
            {
                string str;
                if (hex[0].Equals('#'))
                    str = hex.Substring(1, hex.Length);
                else
                    str = hex.Substring(0, hex.Length);

                c.r = (float)System.Int32.Parse(str.Substring(0, 2),
                    NumberStyles.AllowHexSpecifier) / 255.0f;
                c.g = (float)System.Int32.Parse(str.Substring(2,2),
                    NumberStyles.AllowHexSpecifier) / 255.0f;
                c.b = (float)System.Int32.Parse(str.Substring(4, 2),
                    NumberStyles.AllowHexSpecifier) / 255.0f;

                if (str.Length >= 8)
                {
                    c.a = System.Int32.Parse(str.Substring(6, 1),
                       NumberStyles.AllowHexSpecifier) / 255.0f;
                    Debug.Log(str.Substring(6, 1));
                }
                else c.a = 1.0f;

            }
            catch (Exception e)
            {
                Debug.Log("Could not convert " + hex + " to Color. " + e );
                return new Color(0, 0, 0, 0);
            }
        }
        return c;

    }

    /// <summary>
    /// Converts a rrr,ggg,bbb,aaa string into a Unity color, which is float.
    /// </summary>
    /// <param name="rgba"></param>
    /// <returns></returns>
    public static Color IntegerStringColorToFloatColor(string rgba)
    {
        Color c = new Color();
        if (rgba != null && rgba.Length > 0)
        {
            string[] s = rgba.Split(',');

            c.r = float.Parse(s[0]) / 255f;
            c.g = float.Parse(s[1]) / 255f;
            c.b = float.Parse(s[2]) / 255f;
            c.a = float.Parse(s[3]) / 255f;
        }

        return c;
    }

    /// <summary>
    /// Converts a Array into a string. The items are sperated through ", ".
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="arr"></param>
    /// <returns></returns>
    public static string ArrayToString<T>(this T[] arr)
    {
        StringBuilder sb = new StringBuilder();
        foreach (T item in arr)
        {
            sb.Append(item.ToString() + ", ");
        }
        sb.Remove(sb.Length - 2, 2);
        return sb.ToString();
    }

    /// <summary>
    /// Fills array <para>arr</para> with value <para>value</para>.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="arr"></param>
    /// <param name="value"></param>
    public static void PopulateArray<T>(this T[] arr, T value)
    {
        for (int i = 0; i < arr.Length; i++)
        {
            arr[i] = value;
        }
    }

    /// <summary>
    /// Reads a text file and returns the content as a string.
    /// </summary>
    /// <param name="pathAndName"></param>
    /// <returns></returns>
    public static string ReadTextFile(string pathAndName)
    {
        string dataAsString = "";
        try
        {
            StreamReader textReader = File.OpenText(pathAndName);
            dataAsString = textReader.ReadToEnd();
            textReader.Close();
        }
        catch (Exception e)
        {
            return e.Message;
        }
        return dataAsString;
    }

    /// <summary>
    /// Converts <para>str</para> to a byte array and returns the array.
    /// </summary>
    /// <param name="str"></param>
    /// <returns></returns>
    public static byte[] StringToBytes(string str)
    {
        byte[] bytes = new byte[str.Length * sizeof(char)];
        System.Buffer.BlockCopy(str.ToCharArray(), 0, bytes, 0, bytes.Length);
        return bytes;
    }

    /// <summary>
    /// Converts a byte array into a string and returns the string.
    /// </summary>
    /// <param name="bytes"></param>
    /// <returns></returns>
    public static string BytesToString(byte[] bytes)
    {
        char[] chars = new char[bytes.Length / sizeof(char)];
        System.Buffer.BlockCopy(bytes, 0, chars, 0, bytes.Length);
        return new string(chars);
    }

}