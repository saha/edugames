// filename InputTabNavigation.cs
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections.Generic;

/// <summary>
/// Implementation of a simple Tab Navigation for Selectables.
///	To use this script. Attach it to the parent of the Selectables you want to Navigate. For example the canvas or panel.
/// </summary>
public class InputTabNavigation : MonoBehaviour {

	// List of all selectables
    private List<Selectable> elements;

    // Index of the current selected object
    [HideInInspector]
    private static int index = 0;

    /// <summary>
    /// Init
    /// </summary>
	private void Start()
    {
    	// Get all Selectable Childs of the gameObject this script is attached to and save them in elements.
        elements = new List<Selectable>();
        for (int i = 0; i < transform.childCount; i++)
        {
            Transform child = transform.GetChild(i);
            if (child)
            {
                Selectable s = transform.GetChild(i).GetComponent<Selectable>();
                if (s)
                {
                    elements.Add(s);
                }
            }
        }

    }

	/// <summary>
	/// Unity Update function
	/// </summary>
    public void Update()
    {
        if(Input.GetKeyDown(KeyCode.Tab))
        {
        	// On Tab select the next element in the list and inc index.
            if (index == elements.Count) index = 0;
            elements[index].Select();
            index++;
        }
    }
}
