// Filename: Pair.cs

/// <summary>
/// Simple version of the c++ class std::pair
/// </summary>
/// <typeparam name="T"></typeparam>
/// <typeparam name="U"></typeparam>
public class Pair<T,U> {
    public Pair()
    {  
    }

    public Pair(T first, U second) {
        this.First = first;
        this.Second = second;
    }

    public T First { get; set; }
    public U Second { get; set; }

    new public string ToString()
    {return this.First + "," + this.Second;}

}
