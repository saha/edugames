// filename: ResetRoundScript.cs
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// This script is called by the "Alles nach Oben" Button
/// It put every Letter from the bottom layout back to the top layout and "restarts the round".
/// </summary>
    public class ResetRoundScript : MonoBehaviour
    {
        // the top layout.
        public Transform layoutTop;
        // the bottom layout.
        public Transform layoutBottom;

        /// <summary>
        /// Function for the ResetButton.
        /// A simpil algorythm to sort all letters back into the top layout by using a stack.
        /// </summary>
        public void Button_ResetRound()
        {
            Debug.Log("ResetRound");
            // create a stack.
            Stack<Transform> rest = new Stack<Transform>();

            // go through the layouts.
            for (int i = 0; i < layoutTop.childCount; i++)
            {
                // get the content of the slots with index i
                Transform bot = layoutBottom.GetChild(i).transform;   // layoutBottom/Slot/Letter
                Transform top = layoutTop.GetChild(i).transform;    // layoutTop/Slot/Letter

                // check if one slot was empty
                bot = CheckForChild(bot);
                top = CheckForChild(top);
             
                // Top was empty and bot was not.
                    if (!top && bot)
                    {
                        Debug.Log("Case1: top empty, bot full!");
                        bot.SetParent(layoutTop.GetChild(i).transform, false);  // put the the bot letter into the top slot.
                    }
                    // both slots were not empty.
                    else if (bot  && top)
                    {
                        Debug.Log("Case2: both full");
                        // put bot into the stack.
                        rest.Push(bot);
                        bot.SetParent(null, false);
                    }
            }

            Debug.Log(rest.Count + " remaining... ");
            // go through the layouts again. Now we empty the stack
            for (int i = 0; i < layoutBottom.childCount; i++)
            {
                // get the top slot.
                Transform top = layoutTop.GetChild(i).transform;

                // check if top is empty.
                top = CheckForChild(top);          
                // check it the stack is empty
                    if (rest.Count > 0)
                        if (!top) // top was empty
                            rest.Pop().SetParent(layoutTop.GetChild(i), false);// put the first item from the stack into top.

            }
        }

        /// <summary>
        /// Helper Method to check if a object has children. If true return the first child.
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        private Transform CheckForChild(Transform obj)
        {
            if (obj.childCount > 0)
                return obj.GetChild(0).transform;
            else
                return null;
        }
    }
