// filename: StatePlaying.cs
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// This script loads the levels and controlls the game play.
/// </summary>
    public class StatePlaying : GameState
    {
        // the solution of the current problem.
        public static string TheWord = "";

        // The List of every word of every level.
        public static List<Pair<int, List<string>>> words = new List<Pair<int, List<string>>>();

        // prefab for a letter.
        public GameObject charPrefab;

        // prefab for a slot.
        public GameObject slotPrefab;

        // prefab for the layout on the top.
        public LayoutGroup layoutTop;

        // prefab for the layout on the bottom.
        public LayoutGroup layoutBottom;

        // Reference to another script.
        private WordLoader wordLoader;

        /// <summary>
        /// Init.
        /// </summary>
        private void Start()
        {
            // get the components from the gameobjects and save them.
            layoutBottom = layoutBottom.GetComponent<LayoutGroup>();
            layoutTop = layoutTop.GetComponent<LayoutGroup>();
            wordLoader = GetComponent<WordLoader>();

            // load all words
            wordLoader.LoadWords();
        }

        /// <summary>
        /// Called from WinconditionHandler when win conditions are met.
        /// </summary>
        public void RoundWon()
        {
            gameManager.NewGameState(gameManager.stateAfterRound);
        }

        /// <summary>
        /// Implementation of the OnStateEntered Method.
        /// </summary>
        public override void OnStateEntered()
        {
            // activate the UI layout elements.
            layoutTop.gameObject.SetActive(true);
            layoutBottom.gameObject.SetActive(true);

            // start a new round.
            NewRound();
        }

        /// <summary>
        /// Implementation of the OnStateQuit Method.
        /// Basicly we do clean up work here.
        /// </summary>
        public override void OnStateQuit()
        {
            // delete the slots
            DeleteSlots();
            // deactivate layout elements.
            layoutTop.gameObject.SetActive(false);
            layoutBottom.gameObject.SetActive(false);
        }

        /// <summary>
        /// Implementation of the StateUpdate Method.
        /// </summary>
        public override void StateUpdate()
        {
            // On Esc change into the inputstate.
            if (Input.GetKeyUp(KeyCode.Escape))
            {
                gameManager.NewGameState(gameManager.stateInput);
            }
        }

        /// <summary>
        /// Shuffles the given word and puts the letters into slots.
        /// </summary>
        /// <param name="word"></param>
        private void CreateWord(string word)
        {
            // get the position of the char prefab.
            Vector3 pos = charPrefab.transform.position;
            // copy the letters in to a char array
            char[] shuffled = word.ToCharArray();
            // remember the original word and mark it as TheWord to find.
            TheWord = word;
            // shuffle the array.
            Utils.Shuffle<char>(shuffled);

            // create for every letter a charprefab and place it at the right spot
            for (int i = 0; i < word.Length; i++)
            {
                // instantiate a char prefab (Slot with Text as child. This child has drag and click handler allready attached)
                GameObject panel = Instantiate(charPrefab, pos, charPrefab.transform.rotation) as GameObject;
                // put the instantiated object into the top layout.
                panel.transform.SetParent(layoutTop.transform, false);

                // set the text of the prefab.
                Text text = panel.transform.GetChild(0).GetComponent<Text>() as Text;
                text.text = shuffled[i].ToString();
                text.name = shuffled[i].ToString();
            }

            // create empty slots in the bottom layout.
            CreateSlots(word);
        }

        /// <summary>
        /// Creates word.lenght empty slots in the bottom layout.
        /// </summary>
        /// <param name="word"></param>
        private void CreateSlots(string word)
        {
            Vector3 pos = slotPrefab.transform.position;

            for (int i = 0; i < word.Length; i++)
            {
                // instantiate a slot prefab.
                GameObject panel = Instantiate(slotPrefab, pos, slotPrefab.transform.rotation) as GameObject;
                // put the prefab into the bottom layout.
                panel.transform.SetParent(layoutBottom.transform, false);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public void NewRound()
        {
            // remember the index of the current word in the list of lists (words[level][currentWordIndex])
            int currentWordIndex;
          
            // Clean the field
            DeleteSlots();

            // Max level isn't reached yet.
            if (!StateAfterRound.maxLevelReached)
            {
                // get a random word.
                currentWordIndex = Random.Range(0, words[GameData.Level].Second.Count - 1);
                // Create the chosen word.
                CreateWord(words[GameData.Level].Second[currentWordIndex]);
              
            }             
            else // max level is reached. Now the Level doesn't matter anymore
            {
                Debug.Log("MaxLevel Mode");
                int randLevel = Mathf.Max(0,Random.Range(0, words.Count-1));
                currentWordIndex = Mathf.Max(0, Random.Range(0, words[randLevel].Second.Count - 1));
                CreateWord(words[Random.Range(0, words.Count)].Second[currentWordIndex]);

            }
            
        }

        /// <summary>
        /// Clean up function. 
        /// Removes every slot.
        /// </summary>
        private void DeleteSlots()
        {
            for (int i = 0; i < layoutTop.transform.childCount; i++)
            {
                Destroy(layoutTop.transform.GetChild(i).gameObject);
                Destroy(layoutBottom.transform.GetChild(i).gameObject);
            }
        }
    }
