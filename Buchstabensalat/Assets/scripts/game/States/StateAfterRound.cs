// file: StateAfterRound.cs

using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// This state is entered as soon as the player as solved the word. (currentSolution == TheWord, in WinconditionHandler) 
/// Whis script displays a small UI that tells the player that he solved the word. 
/// After then player confirms, this script transitions into the next state (new round/ new level)
/// This script also handles updating the points/levels displays in the UI.
/// </summary>
    public class StateAfterRound : GameState
    {
        // the UI
        public GameObject winUI;

        // UI element displaying the points
        public Text textPoints;
        // UI element displaying the levels
        public Text textLevel;

        // This boolean is true is GamaData.Level > StatePlaying.words.Count-1
        public static bool maxLevelReached;

        private void Start()
        {
            textLevel = textLevel.GetComponent<Text>();
            textPoints = textPoints.GetComponent<Text>();
        }

        /// <summary>
        /// Implementation of the abstract method inherited by GameState
        /// </summary>
        public override void OnStateEntered()
        {
            // activate the UI.
            winUI.SetActive(true);

            // Increment and update the points.
            GameData.UpdatePointLable(textPoints, true);
            // Increment the progresstion which is necessary to reach the next level.
            GameData.LevelProgression++;

            // We have enough progression to reach the new level
            if (GameData.LevelProgression == LevelConfig.Points_For_Next_Level)
            {
                // reset the progression
                GameData.LevelProgression = 0;
                // increment and update the level
                GameData.UpdateLevelLable(textLevel, true);
            }
            Debug.Log(GameData.Level + ", " + StatePlaying.words.Count);
            // Check if we reached the maximum level.
            if (GameData.Level > StatePlaying.words.Count-1)
                {
                    maxLevelReached = true;
                    Debug.Log("The highest level has been reached entering endless mode.\n Using now words from every level randomly.");
                }
            else
                maxLevelReached = false;
        }

        /// <summary>
        /// Implementation of the abstract method inherited by GameState
        /// </summary>
        public override void OnStateQuit()
        {
            winUI.SetActive(false);
        }


        /// <summary>
        /// Implementation of the abstract method inherited by GameState
        /// </summary>
        public override void StateUpdate()
        {
        }

        /// <summary>
        /// Function for the NextButton
        /// </summary>
        public void Button_Next()
        {
            // Transition into the next game state.
            gameManager.NewGameState(gameManager.statePlaying);
        }

    }
