// file: WordLoader.cs

// This is necessary for web builds.
// Web builds don't support all Systen.IO methods like Windows. Also the Paths are different.
#if !UNITY_WEBPLAYER
#define SAVE_OK
#endif

    using System.Collections.Generic;
    using System.IO;
    using System.Linq; // So usefull!
    using UnityEngine;

/// <summary>
/// Script that loads every level from every txt file meeting the criteria and writes them into StatePlaying.words
/// </summary>
    public class WordLoader : MonoBehaviour
    {
        public static int MaxWordLenght = 20;

        public void LoadWords()
        {
#if SAVE_OK
            Debug.Log("Load words...");
            int lvl = 0;
            int wordCount = 0;
            // Get all txt files from the level directory
            DirectoryInfo dir = new DirectoryInfo(LevelConfig.Levels_Directoy);

            // Sort all Levels by their name and remove all files containung .meta in it.
            // Order is descending ( 0 ist first 1 is second 2 third and so on).
            FileInfo[] files = dir.GetFiles()
                .Where(val => !val.Name.Contains(".meta"))
                .OrderBy(p => p.Name)
                .ToArray();

            foreach (var file in files)
            {
                // get the content of the file
                string content = File.ReadAllText(file.FullName);
                // create temp list.
                List<string> temp = new List<string>();
                // get every word in the file without ','.
                foreach (string word in content.Split(','))
                {
                    // make sure to remove spaces
                    string tempWord = word.Trim();
                    // make sure the word doesn't exceed the cap.
                    if (tempWord.Length > MaxWordLenght)
                    {
                        // the word was to long and will be capped.
                        Debug.Log("to long: " + word + ", " + tempWord.Length + "\n Capping the word to " + MaxWordLenght + " character.");
                        tempWord = tempWord.Substring(0, MaxWordLenght);
                    }
                    // add the word the to temp list.
                    temp.Add(tempWord);
                    wordCount++;
                }
                // add the temp list to the List of Lists.
                StatePlaying.words.Add(new Pair<int, List<string>>(lvl, temp));
                lvl++;
            }
            Debug.Log("Loaded " + wordCount + " words in " + lvl + " levels.");
#endif
        }
    }
