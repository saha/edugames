// filename: CreateDefaultLevels.cs
using UnityEngine;
using System.Collections;
using System.IO;

/// <summary>
/// A script to create level files from resources after the built.
/// </summary>
    public class CreateDefaultLevels : MonoBehaviour
    {
        /// <summary>
        /// Init
        /// </summary>
        private void Start()
        {
            CreateDefaultLevelFiles();
        }

        /// <summary>
        /// Reads Level files from resources and saves them into a txt files. 
        /// </summary>
        private void CreateDefaultLevelFiles()
        {
            // Check if level directory exitsts.
            if (!Directory.Exists(LevelConfig.Levels_Directoy))
                Directory.CreateDirectory(LevelConfig.Levels_Directoy);

            // read all level files
            for (int i = 0; i < 2; i++)
            {
                string file = LevelConfig.Path_And_Filename + "_" + i.ToString() + ".txt";
                // only create files if they do not exist.
                if (!File.Exists(file))
                {
                    TextAsset ta = Resources.Load("Levels/Level_" + i.ToString() ) as TextAsset;
                    Debug.Log(ta.text);
                    File.AppendAllText(file, ta.text);
                }
            }
        }
    }

