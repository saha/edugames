using UnityEngine;
using UnityEngine.EventSystems;


    public class DragHandler : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler
    {
        public static GameObject itemBeingDraged;
        public static GameObject itemClicked;
        public static Vector3 startPosition;
        public static Transform startParent;

        public void OnBeginDrag(PointerEventData eventData)
        {
            itemBeingDraged = gameObject;
            startPosition = transform.position;
            startParent = transform.parent;
            GetComponent<CanvasGroup>().blocksRaycasts = false;
        }

        public void OnDrag(PointerEventData eventData)
        {
            transform.position = Input.mousePosition;
        }

        public void OnEndDrag(PointerEventData eventData)
        {
            itemBeingDraged = null;
            if (transform.parent == startParent)
            {
                transform.position = startPosition;
            }
            GetComponent<CanvasGroup>().blocksRaycasts = true;
        }
    }
