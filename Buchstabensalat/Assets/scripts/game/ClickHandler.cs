// filename: ClickHandler.cs
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;


/// <summary>
/// This script is used to change the contents of slota via clicks.
/// </summary>
    public class ClickHandler : MonoBehaviour, IPointerClickHandler
    {
        // the item which was clicked.
        public static GameObject itemClicked;
        // start position of the item.
        public static Vector3 startPosition;
        // start parent of the item
        public static Transform startParent;

        /// <summary>
        /// Implementation of the IPointerClickHandler interface.
        /// This is called as soon as you click in the item this script is attached to.
        /// </summary>
        /// <param name="eventData"></param>
        public void OnPointerClick(PointerEventData eventData)
        {
            if (itemClicked) // if we allready have click on a object. So we switch the contents.
            {
                // highlight the clicked item.
                itemClicked.transform.parent.GetComponent<Image>().color = Utils.White;

                // set the new parent for the previous clicked item.
                ClickHandler.itemClicked.transform.SetParent(transform.parent);
                // set the new position for the previous clicked item.
                ClickHandler.itemClicked.transform.position = transform.parent.position;

                // set the parent for the clicked item.
                transform.SetParent(ClickHandler.startParent);
                // set the postion for the clicked item.
                transform.position = ClickHandler.startPosition;

                // Don't forget this!
                itemClicked.GetComponent<CanvasGroup>().blocksRaycasts = true;
                // reset the clicked item.
                itemClicked = null;

                // call IHasChanged.
                ExecuteEvents.ExecuteHierarchy<IHasChanged>(gameObject, null, (x, y) => x.HasChanged());
                    //(gameObject, null, (x, y) => x.HasChanged());
            }
            else // we don't have a item selected.
            {
                // "select" the item.
                // save the item we clicked.
                itemClicked = gameObject;
                // save the start position.
                startPosition = transform.position;
                // save the start parent.
                startParent = transform.parent;
                // highlight the item we clicked.
                transform.parent.GetComponent<Image>().color = Color.black;

                // Don't forget this!
                GetComponent<CanvasGroup>().blocksRaycasts = false;
            }
        }
    }
