// file: Slot.cs
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;


/// <summary>
/// This script enables the GameObject it is attached to to receive Drop Events.
/// Attach this script to every GameObject you want to drop other GameObjects onto. 
/// This Script works togather witch DropHandler Scripts.
/// </summary>
    public class Slot : MonoBehaviour, IDropHandler, IPointerClickHandler
    {
        /// <summary>
        /// Returns the first child of the slot. Which is the thing we are going to drag into another slot.
        /// </summary>
        public GameObject content
        {
            get
            {
                if (transform.childCount > 0)
                    return transform.GetChild(0).gameObject;

                return null;
            }
        }

        /// <summary>
        /// Implemntation of the IDropHandlerInterface
        /// </summary>
        /// <param name="eventData"></param>
        void IDropHandler.OnDrop(PointerEventData eventData)
        {
            if (!content) // Dropped on a empty slot
            {
                // set new parent
                DragHandler.itemBeingDraged.transform.SetParent(transform);
                // set new position
                DragHandler.itemBeingDraged.transform.position = transform.position;
                // call IHasChanged
                ExecuteEvents.ExecuteHierarchy<IHasChanged>(gameObject, null, (x, y) => x.HasChanged());
            }
            else // there is akkready content in the slot. So we switch them.
            {
                // create a temp copy
                GameObject temp = content;

                // set the new parent
                DragHandler.itemBeingDraged.transform.SetParent(transform);
                // set the new position
                DragHandler.itemBeingDraged.transform.position = transform.position;

                // put the copy in the old spot
                temp.transform.SetParent(DragHandler.startParent);
                temp.transform.position = DragHandler.startPosition;
                // call IHasChanged
                ExecuteEvents.ExecuteHierarchy<IHasChanged>(gameObject, null, (x, y) => x.HasChanged());
            }
        }

        /// <summary>
        /// Implementation of the IPointerClickHandler. 
        /// This Method works togather with the <c>ClickHandler</c>
        /// Used to changed the content of slots via clicks.
        /// </summary>
        /// <param name="eventData"></param>
        void IPointerClickHandler.OnPointerClick(PointerEventData eventData)
        {
            if (!content)
            {
                // highlight the clicked object.
                ClickHandler.itemClicked.transform.parent.GetComponent<Image>().color = Utils.White;
                // set new parent
                ClickHandler.itemClicked.transform.SetParent(transform);
                // set new position
                ClickHandler.itemClicked.transform.position = transform.position;
                ClickHandler.itemClicked.GetComponent<CanvasGroup>().blocksRaycasts = true;
                ClickHandler.itemClicked = null;
                ExecuteEvents.ExecuteHierarchy<IHasChanged>(gameObject, null, (x, y) => x.HasChanged());
            }
        }
    }
