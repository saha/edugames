// filename: WinconditionHandler.cs
using UnityEngine;
using UnityEngine.EventSystems;


/// <summary>
/// This script is called by Slot-scripts and checks if the winconditions are met.
/// </summary>
    public class WinconditionHandler : MonoBehaviour, IHasChanged
    {
        public StatePlaying stateplaying;

        /// <summary>
        /// The characters in the slots of the bottom layout.
        /// </summary>
        private string currentSolution;

        /// <summary>
        /// Init
        /// </summary>
        private void Start()
        {
            stateplaying = stateplaying.GetComponent<StatePlaying>();
        }

        /// <summary>
        /// Implementation of the WinconditionHandler.
        /// This event is triggered by OnDrop and onClick
        /// </summary>
        void IHasChanged.HasChanged()
        {
            currentSolution = "";
            // Check which letters are in which slots.
            for (int i = 0; i < stateplaying.layoutBottom.transform.childCount; i++)
            {
                Transform trans = stateplaying.layoutBottom.transform.GetChild(i);
                GameObject letter = trans.GetComponent<Slot>().content;

                // Build the letters to a word.
                if (letter)
                    currentSolution += letter.name[0];
                else
                    currentSolution += "_";
            }
            Debug.Log("CurrentSolution = " + currentSolution + "\n" + "TheWord = " + StatePlaying.TheWord);

            // Compare the current solution and the winning word.
            if (currentSolution.ToLower() == StatePlaying.TheWord.ToLower())
                stateplaying.RoundWon();
        }
    }

// Declare new Interface
namespace UnityEngine.EventSystems
{
    public interface IHasChanged : IEventSystemHandler
    {
        void HasChanged();
    }
}