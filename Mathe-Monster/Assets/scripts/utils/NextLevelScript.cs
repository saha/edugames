using UnityEngine;
using UnityEngine.UI;

public class NextLevelScript : MonoBehaviour
{

   public GameManager gameManager;
   public GameObject animationPicture;
   public GameObject airShipPicture; 
   public Image handle;

   private float time;
    /// <summary>
    /// Initialization of members.
    /// </summary>
    void Start()
    {
        gameManager = gameManager.GetComponent<GameManager>();
        handle = handle.GetComponent<Image>();
        animationPicture = animationPicture.gameObject;
        airShipPicture = airShipPicture.gameObject;

        animationPicture.SetActive(false);
    }

    /// <summary>
    /// Activates the picture for the animation
    /// </summary>
    public void StartNextLevelAnimation(int time)
    {
        handle.transform.gameObject.SetActive(false);

        animationPicture.SetActive(true);

        if (animationPicture.transform.localScale.x < 2.0f)
            animationPicture.transform.localScale = new Vector3(1 + GameData.Level * 0.1f,
                                                                1 + GameData.Level * 0.1f, 1);
        this.time = time;
    }

    /// <summary>
    /// This "Update" Function is called from the GameManagers own Update Function.
    /// This Function slowly translates an Image from one side of the screen to the other side.
    /// </summary>
    public void UpdateNextLevelCountDown()
    {
        this.time -= Time.deltaTime;
        animationPicture.GetComponent<Animator>().Play("monster");
        airShipPicture.transform.Rotate(Vector3.forward, Time.deltaTime * 200);
        if (time <= 0)
        {
            CleanUp();
            // change of state is handeled by the CountDownScript.
        }
    }
    public void CleanUp()
    {
        airShipPicture.transform.rotation = new Quaternion(0, 0, 0, 0);
        animationPicture.transform.position = new Vector3(20f, 0, 0);
        animationPicture.SetActive(false);
        handle.transform.gameObject.SetActive(true);
    }
}
