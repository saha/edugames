// filename: TakeScreenShot
using UnityEngine;
using System;
using System.Collections;
using System.IO;

/// <summary>
/// A Script that takes a screenshot from the maincamera and saves it into <c>screenShotDir</c>.
/// Taken from Smith's and Queiroz's Unity 4.x cookbook, p. 225.
/// Added Custom Directory
/// </summary>
public class TakeScreenShot : MonoBehaviour {

    public string prefix = "Screenshot";
    public string screenShotDir;
    public int scale = 1;
    public bool useReadPixels = false;
    private Texture2D texture;

    /// <summary>
    /// Init
    /// </summary>
    private void Start()
    {
        screenShotDir = Path.GetDirectoryName(Application.dataPath) + Path.DirectorySeparatorChar + "Screenshots";
    }

	
    /// <summary>
    /// Unioty Update funtion
    /// </summary>
	private void Update () {
        if (Input.GetKeyDown(KeyCode.P))
            StartCoroutine(TakeShot());
	}

    /// <summary>
    /// Coroutine 
    /// </summary>
    /// <returns></returns>
    private IEnumerator TakeShot()
    {
        // Check if the dir exists, if not create it.
        if (!Directory.Exists(screenShotDir))
            Directory.CreateDirectory(screenShotDir);

        string date = System.DateTime.Now.ToString("_d.MMM.yyyy HH-mm");
        int screenHeight = Screen.height;
        int screenWidth = Screen.width;

        Rect sRect = new Rect(0, 0, screenWidth, screenHeight);

        if (useReadPixels)
        {
            yield return new WaitForEndOfFrame();
            texture = new Texture2D(screenWidth, screenHeight, TextureFormat.RGB24, false);
            texture.ReadPixels(sRect, 0, 0);
            texture.Apply();
            byte[] bytes = texture.EncodeToPNG();
            Destroy(texture);
            File.WriteAllBytes(screenShotDir + Path.DirectorySeparatorChar + prefix + date + ".png", bytes);
        }
        else
        {
            // Build in Unity ScreenshotCapture
            Application.CaptureScreenshot(screenShotDir + Path.DirectorySeparatorChar + prefix + date + ".png", scale);
        }
        Debug.Log("Screenshot taken: " + prefix + date +".png");
    }
}
