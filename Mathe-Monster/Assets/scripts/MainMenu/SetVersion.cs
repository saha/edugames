using UnityEngine;
using UnityEngine.UI;

public class SetVersion : MonoBehaviour {

	// Use this for initialization
	void Start () {
        string version = GameConfig.Application_Version;
        Debug.Log("Version " + version + " detected.");
        Text text = gameObject.GetComponent<Text>();
        text.text += version;
	}
}
