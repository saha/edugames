// filename Oscillator.cs
using UnityEngine;
using UnityEngine.UI;


/// <summary>
/// A Simple OscillatorScrip, that lets the GameObject which this script is attached to circle around. 
/// </summary>
public class Oscillator : MonoBehaviour
{

    // range
    public float factor = 1;

    // direction
    public int clockwise = 1;
    public bool randomOscillateDirection = true;

    // speed
    public float oscillateSpeed = 1f;
    public bool randomOscillateSpeed = true;


    private float timeCounter = 0f;

    /// <summary>
    /// Init
    /// </summary>
    private void Start()
    {
        // check for random direction and set it.
        if (randomOscillateDirection)
        {
            clockwise = Random.Range(0, 2);
            if (clockwise == 0)
                clockwise = -1;
        }

        // check for random speed and set it
        if (randomOscillateSpeed)
        {
            oscillateSpeed = Random.Range(0.01f, 0.05f);
            if (clockwise == 0)
                clockwise = -1;
        }
    }

    /// <summary>
    /// Unity Update function
    /// </summary>
    void Update()
    {
        timeCounter += Time.deltaTime;

        float x = Mathf.Cos(timeCounter) * clockwise * oscillateSpeed * factor;
        float y = Mathf.Sin(timeCounter) * clockwise * oscillateSpeed * factor;
        float z = Mathf.Sin(timeCounter) * 0.001f * factor;

        transform.Translate(new Vector3(x, y, z));
    }
}
