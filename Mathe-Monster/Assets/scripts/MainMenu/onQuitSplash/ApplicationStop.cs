// filename: ApplicationStop.cs
using UnityEngine;
using System.Collections;

/// <summary>
/// This script cloes the Application after <c>showSplashTimeout</c> seconds or after any key was pressed.
/// </summary>
public class ApplicationStop : MonoBehaviour {

    public int showSplashTimeout = 2;

    private bool allowQuitting = false;

    // Name of the Scene containing the spash screen.
    private string finalScene = "onQuitSplash";
	
    /// <summary>
    /// Init
    /// </summary>
    public void OnApplicationQuit()
    {
        Debug.Log("OnApplicationStop");
        if (Application.loadedLevelName.ToLower() != finalScene.ToLower())
        {
            StartCoroutine("DelayedQuit");
        }
        if (!allowQuitting)
        {
            Application.CancelQuit();
        }
        else
            Application.Quit();
    }

    /// <summary>
    /// Coroutine
    /// </summary>
    /// <returns></returns>
    IEnumerator DelayedQuit()
    {
        Debug.Log("Corutine Started Waiting " +  showSplashTimeout +" secs.");
        Application.LoadLevel(finalScene);
        yield return new WaitForSeconds(showSplashTimeout);
        Debug.Log("Time OVER");
        allowQuitting = true;
        Application.Quit();
    }
}
