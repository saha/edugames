// filename: LevelXMLParser.cs
using UnityEngine;
using System;
using System.Xml;
using System.IO;
using System.Collections.Generic;

/// <summary>
/// Used to Read XML files and parse the data into <c><LevelConfig/c>.
/// </summary>
public class LevelXMLParser : MonoBehaviour
{
    // Declare InfixPostfixEvaluator
    private InfixPostfixEvaluator infixPostfixEvaluator;

    /// <summary>
    /// Init.
    /// </summary>
    private void Start()
    {
        // create InfixPostfixEvaluator
        infixPostfixEvaluator = new InfixPostfixEvaluator();
    }

    /// <summary>
    /// Reads level <para>level</para> and pareses it into <c>LecvelConfig</c>.
    /// </summary>
    /// <param name="level"></param>
    public void LoadLevel(int level)
    {
        // check for max level
        if (level >= CountLevels())
        {
            Debug.Log("Max Level reached.");
            level = CountLevels();
            GameData.Level = level;
        }
        // clear the privious list.
        LevelConfig.Equations.Clear();
        

        // Get the Path
        string lvl = LevelConfig.Path_And_Filename + "_" + level.ToString() + ".xml"; // .../Resources/Levels/Level_X.xml
        Debug.Log("Loading Level " + lvl.ToString());
        // Load the File.
        string data = Utils.ReadTextFile(lvl);

        // start parsing
        ParseAndSetLevelFromXML(data);
    }

    /// <summary>
    /// Parses a xml string into <c>LevelConfig</c>
    /// </summary>
    /// <param name="xmlText"></param>
    private void ParseAndSetLevelFromXML(string xmlText)
    {
        // create new XMLDoc
        XmlDocument xmlDoc = new XmlDocument();
        // load the string into theXMLDoc.
        xmlDoc.Load(new StringReader(xmlText));

        // set the properties
        SetLevelProperty(xmlDoc.FirstChild);
    }

    /// <summary>
    /// Small helper function to count the amount of different level files.
    /// </summary>
    /// <returns></returns>
    private int CountLevels()
    {
        return Directory.GetFiles(LevelConfig.Levels_Directoy).Length;
    }

    /// <summary>
    /// Goes through the xml file and sets read values into LevelConfig variables.
    /// </summary>
    /// <param name="levelNode"></param>
    private void SetLevelProperty(XmlNode levelNode)
    {
        XmlNode rainNode = levelNode.FirstChild;
        SetRainProperites(rainNode);

        XmlNode progressNode = rainNode.NextSibling;
        LevelConfig.Points_For_Next_Level = Convert.ToInt32(progressNode.Attributes.Item(0).InnerXml);

        XmlNode range = progressNode.NextSibling;
        LevelConfig.Min_Slider_Value = Convert.ToInt32(range.Attributes.Item(0).InnerXml);
        LevelConfig.Max_Slider_Value = Convert.ToInt32(range.Attributes.Item(1).InnerXml);      

        XmlNode equationsNode = range.NextSibling;

        for (int i = 0; i < equationsNode.Attributes.Count; i++)
        {
            string e = equationsNode.Attributes.Item(i).InnerXml.ToString();
            int res = (int)infixPostfixEvaluator.Calculate(e);
           // Debug.Log(e +"="+res);
            LevelConfig.Equations.Add(new Pair<string, int>(e, res));
        }
    }

    /// <summary>
    /// Pareses a part of the xml file and sets the values.
    /// </summary>
    /// <param name="rainNode"></param>
    private void SetRainProperites(XmlNode rainNode)
    {
        XmlNode amount = rainNode.FirstChild;
        LevelConfig.Amount_of_Falling_Numbers = Convert.ToInt32(amount.Attributes.Item(0).InnerXml);

        XmlNode speed = amount.NextSibling;
        LevelConfig.Min_Fall_Speed = Convert.ToInt32(speed.Attributes.Item(0).InnerXml);
        LevelConfig.Max_Fall_Speed = Convert.ToInt32(speed.Attributes.Item(1).InnerXml);

        XmlNode drag = speed.NextSibling;
        LevelConfig.Min_Drag = Convert.ToInt32(drag.Attributes.Item(0).InnerXml);
        LevelConfig.Max_Drag = Convert.ToInt32(drag.Attributes.Item(1).InnerXml);
    }
}
