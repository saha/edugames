// filename: ConfigXMLParser.cs
using UnityEngine;
using System;
using System.IO;
using System.Xml;

/// <summary>
/// Used to Read XML files and parse the data into <c><GameConfig/c>.
/// </summary>
public class ConfigXMLParser : MonoBehaviour {

    /// <summary>
    /// Init
    /// </summary>
    private void Start()
    {
        LoadConfigFromFile();
    }

    /// <summary>
    /// Public access point for other scripts.
    /// </summary>
    public void LoadConfigFromFile()
    {
        Debug.Log("Loading Config...");
        string data = Utils.ReadTextFile(GameConfig.Path_And_Filename);
        ParseAndSetConfigFromXML(data);
    }

    /// <summary>
    /// Loads the xml file and starts the parsing.
    /// </summary>
    /// <param name="xmlText"></param>
    private void ParseAndSetConfigFromXML(string xmlText)
    {
        // create a new xml doc.
        XmlDocument xmlDoc = new XmlDocument();
        // fill the xml doc.
        xmlDoc.Load(new StringReader(xmlText));

        // set the first node.
        XmlNode node = xmlDoc.FirstChild;
        // start parsing.
        SetConfigProperty(node);
    }

    /// <summary>
    /// Parses a part of the xml file into <c>GameData</c>
    /// </summary>
    /// <param name="node"></param>
    private void SetConfigProperty(XmlNode node)
    {
        XmlNode versionNode = node.FirstChild;
        GameConfig.Application_Version = versionNode.InnerXml.ToString();

        XmlNode countDownNode = versionNode.NextSibling;
        SetCountDownProperties(countDownNode);
    }

    /// <summary>
    /// Parses a part of the xml file into <c>GameData</c>
    /// </summary>
    /// <param name="node"></param>
    private void SetCountDownProperties(XmlNode countDownNode)
    {
        XmlNode afterStartNode = countDownNode.FirstChild;
        GameConfig.Time_After_Input = Convert.ToInt32(afterStartNode.InnerXml.ToString());

        XmlNode afterRoundNode = afterStartNode.NextSibling;
        GameConfig.Time_After_Round = Convert.ToInt32(afterRoundNode.InnerXml.ToString());

        XmlNode afterLevelNode = afterRoundNode.NextSibling;
        GameConfig.Time_Ater_Level = Convert.ToInt32(afterLevelNode.InnerXml.ToString());
    }
}
