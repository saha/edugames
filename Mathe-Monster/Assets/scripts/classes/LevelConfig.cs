// filename: LevelConfig.cs
using UnityEngine;
using System.IO;
using System.Collections.Generic;

/// <summary>
/// This is the configuration for the current loaded level.
/// These variables are set by the <c>LevelXMLParser</c>.
/// <remarks>Every level can change every variable.</remarks>
/// </summary>
public static class LevelConfig
{
    // the name of the file.
    // This is an extra variable, because Unity Editor and built Unity Applications use a differnt Path system.
    // Example: 
    // In Editor: Application.dataPath = Assets Folder
    // Built Windows Player:  Application.dataPath = ProjectName_data
    // Also Resources.Load() doesn't want file extentions. 
    public static string Filename = "Level";

    // Path to the directory containing the level files.
    public static string Levels_Directoy = Application.dataPath + Path.DirectorySeparatorChar + "Resources" + Path.DirectorySeparatorChar + "Levels" + Path.DirectorySeparatorChar;
    public static string Path_And_Filename = Levels_Directoy + Filename;

    // The amount of falling numbers to confuse the player.
    public static int Amount_of_Falling_Numbers;

    // the Minimal fallspeed of the falling numbers
    public static int Min_Fall_Speed;
    // the maximal fallspeed of the falling numbers.
    public static int Max_Fall_Speed;

    // another variable that controlls the fall speed.
    public static int Min_Drag;
    // another variable that controlls the fall speed.
    public static int Max_Drag;

    // The amount of points a player need to get IN A ROW to reach the next level.
    public static int Points_For_Next_Level = 5;

    // the min number range.
    public static int Min_Slider_Value;
    // the maximal number range.
    public static int Max_Slider_Value;

    // A list that contains the equations for the current level.
    public static List<Pair<string, int>> Equations = new List<Pair<string, int>>();

    /// <summary>
    /// Overwritten ToString() method.
    /// </summary>
    /// <returns></returns>
    new public static string ToString()
    {
        return Amount_of_Falling_Numbers + ", " + Min_Fall_Speed + ", " + Max_Fall_Speed + ", " + Min_Drag + ", " + Max_Drag + ", " + Points_For_Next_Level;
       
    }
}
