// filename: GameConfig.cs
using UnityEngine;
using System.IO;

/// <summary>
/// A static class containing different game configurations.
/// </summary>
public static class GameConfig {

    // the Version of the Application
    // in the 'Mathemonster' application the version is set via a extern xml file.
    // You can find that xml file under Assets/Resources/Config.xml
    public static string Application_Version;

    // the name of the file.
    // This is an extra variable, because Unity Editor and built Unity Applications use a differnt Path system.
    // Example: 
    // In Editor: Application.dataPath = Assets Folder
    // Built Windows Player:  Application.dataPath = ProjectName_data
    // Also Resources.Load() doesn't want file extentions. 
    public static string Filename = "Config";

    public static string Path_And_Filename = Application.dataPath + Path.DirectorySeparatorChar + "Resources" + Path.DirectorySeparatorChar + Filename + ".xml";


    // Time to wait after a round ended
    public static int Time_After_Round = 3;
    // Time to wait after the player reahed the next level.
    public static int Time_Ater_Level = 11;
    // Time to wait after the InputMenu closes.
    public static int Time_After_Input = 5;
}
