// filename: RemoveNumbers.cs
using UnityEngine;
using System.Collections;

/// <summary>
/// A simple script that destroys the gameobject which colides with THIS gameObject.
/// </summary>
public class RemoveNumbers : MonoBehaviour
{
    // reference to the NumberSpawnScript.
    private NumberSpawnScript NumberSpawn;

    /// <summary>
    /// Init.
    /// </summary>
    void Start()
    {
        // Get the object of the component via find.
        NumberSpawn = GameObject.Find("NumberSpawner").GetComponent<NumberSpawnScript>();
    }

    /// <summary>
    /// Implementation of the OnCollisionEnter Event.
    /// </summary>
    /// <param name="col"></param>
    void OnCollisionEnter(Collision col)
    {
        // remove the falling number from the list
        NumberSpawn.RemoveNumber(col.gameObject);
        // destroy the game object.
        Destroy(col.gameObject);
    }
}
