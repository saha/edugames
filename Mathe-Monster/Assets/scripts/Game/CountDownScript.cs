//file: CountDownScript.cs
using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;

/// <summary>
/// This script controlls the Countdown UI during the "StateWaitingFor" states.
/// </summary>
public class CountDownScript : MonoBehaviour
{
    // reference to the gameManager.
    public GameManager gameManager;
    // the countdown text,
    public Text countDownText;
    // the slider.
    public Image handle;
    // the Countdown UI
    public Canvas countDownCanvas;

    // the time.
    private float time;

    /// <summary>
    /// Init.
    /// </summary>
    void Start()
    {
        // get the components from the GameObjects and save them.
        gameManager = gameManager.GetComponent<GameManager>();
        countDownCanvas = countDownCanvas.GetComponent<Canvas>();
        countDownText = countDownText.GetComponent<Text>();
        handle = handle.GetComponent<Image>();
        // hide the ui.
        countDownCanvas.enabled = false;
    }

    /// <summary>
    /// Starts a new countdown.
    /// </summary>
    /// <param name="time">Time in Seconds</param>
    public void StartCountDown(int time)
    {
        // make the ui visible.
        countDownCanvas.enabled = true;
        // set the time.
        this.time = time;
        // update the text.
        countDownText.text = time.ToString();
        Debug.Log("StartCountDown " + time);
    }

    /// <summary>
    /// Public access point to hide the count down UI.
    /// </summary>
    public void CloseCountdownMenu()
    {
        countDownCanvas.enabled = false;
    }

    /// <summary>
    /// Update function to count the "time" variable down.
    /// </summary>
    public void UpdateCountDown()
    {
        this.time -= Time.deltaTime;
        countDownText.text = Mathf.CeilToInt(time).ToString();
        if (this.time <= 0)
        {
            gameManager.NewGameState(gameManager.stateRoundInProgress);
        }
    }
}
