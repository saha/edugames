// filename: StateInput.cs
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;

/// <summary>
/// This script contains the functions for the Input menu and controlls the flow of it.
/// 
/// <remarks>This is a highly modified version of the template.</remarks>
/// </summary>
public class StateInput : GameState {

    // The input menu
    public Canvas inputMenu;
    public InputField firstNameField;
    public InputField lastNameField;

    // to access Save and Stop Button
    public GameMenuScript gameMenuScript;
    // for loading the level
    public LevelXMLParser levelXMLParser;
    // access the requested number 
    public GameEngine gameEngine;

    // This objects are optional you dont need to use them, but they are there if you want do display the current points, level and player name somewhere.
    [Header("Optional")]
    public Text textPoints;
    public Text textLevel;
    public Text playerName;

	/// <summary>
	/// Init
	/// </summary>
	private void Start() 
    {
        // get the components from the objects and save them.
        gameMenuScript = gameMenuScript.GetComponent<GameMenuScript>();
        levelXMLParser = levelXMLParser.GetComponent<LevelXMLParser>();
        gameEngine = gameEngine.GetComponent<GameEngine>();

        inputMenu = inputMenu.GetComponent<Canvas>();
        firstNameField = firstNameField.GetComponent<InputField>();
        lastNameField = lastNameField.GetComponent<InputField>();

        if (textLevel)
            textLevel = textLevel.GetComponent<Text>();
        if (textPoints)
            textPoints = textPoints.GetComponent<Text>();
        if (playerName)
            playerName = playerName.GetComponent<Text>();
	}

    /// <summary>
    /// Implementation of the OnStateEntered Method.
    /// </summary>
    public override void OnStateEntered() 
    {
        // Make the inputmenu visible
        ShowInputMenu();

        // If there is allready a name in those static variables, then that means, that the player returned into the Inputmenu during the game.
        // Here is a good point to save the GameData.
        if (GameData.FirstName.Length > 0 && GameData.LastName.Length > 0)
        {
            SessionTimer.StopSessionTimer();
            GameData.Save();
        }

        gameMenuScript.DisableButtons();
        gameEngine.ResetRequestedNumber();
    }

    /// <summary>
    /// Implementation of the OnStateQuit Method.
    /// </summary>
    public override void OnStateQuit() 
    {
         // Make sure that there is somethign written in the InputFields.
        if (firstNameField.text.Length > 0 && lastNameField.text.Length > 0)
        {
            // Save the names in a Static Variable.
            GameData.FirstName = firstNameField.text;
            GameData.LastName = lastNameField.text;
        }
        // Try to Load that from that User.
        GameData.Load();

        // Put the name into the UI. This won't cause an error even if there is no Field on the UI.
        UpdatePlayerNameText();

        //Put the Loaded Points and Level into the UI. This won't cause an error even if there are no fields on the UI.
        GameData.UpdatePointLable(textPoints, false);
        GameData.UpdateLevelLable(textLevel, false);

        HideInputMenu();

        gameMenuScript.EnableButtons();
        gameMenuScript.UpdateScoreLabels();
        levelXMLParser.LoadLevel(GameData.Level);
        SessionTimer.StartSessionTimer();
    }

    /// <summary>
    /// Implementation of the StateUpdate Method.
    /// </summary>
    public override void StateUpdate()
    {
        // Some quality of like hotkeys

        // "Return" or enter is the same as pressing the "OK" Button
        if (Input.GetKeyDown(KeyCode.Return))
            Button_OKPressed();

        // "Escape" or Esc is the same as pressing the "cancle" Button.
        if (Input.GetKeyDown(KeyCode.Escape))
            Button_CancledPressed();
    }

    /// <summary>
    /// OnClick Button Handler for the Positive Input Menu Button
    /// </summary>
    public void Button_OKPressed()
    {
        gameManager.NewGameState(gameManager.stateWaitingForStart);
    }

    /// <summary>
    /// OnClick Button Handler for the Negative Button of the Inputmenu. Returns back to the mainmenu.
    /// </summary>
    public void Button_CancledPressed()
    {
        // Loads the scene with the index 0, which is the main menu.
        Application.LoadLevel(0);
    }

    /// <summary>
    /// Function to update the static variabe for the lastname.
    /// </summary>
    /// <param name="t"></param>
    public void InputField_LastName_Changed(string t)
    {
        GameData.LastName = t;
    }

    /// <summary>
    /// Function to update the static variabe for the firstname.
    /// </summary>
    /// <param name="t"></param>
    public void InputField_FirstName_Changed(string t)
    {
        GameData.FirstName = t;
    }

    /// <summary>
    /// Function to set Player name into the optional <c>playerName</c>.
    /// </summary>
    private void UpdatePlayerNameText()
    {
        if (playerName)
            playerName.text = GameData.FirstName + " " + GameData.LastName;
    }

    /// <summary>
    /// Activats the Inputmenu and insters First and Lastname if there are some.
    /// </summary>
    private void ShowInputMenu()
    {
        if (GameData.FirstName.Length > 0 && GameData.LastName.Length > 0)
        {
            firstNameField.text = GameData.FirstName;
            lastNameField.text = GameData.LastName;
        }
        inputMenu.enabled = true;
    }

    /// <summary>
    /// Disables the inputmenu.
    /// </summary>
    private void HideInputMenu()
    {
        inputMenu.enabled = false;
    }
}
