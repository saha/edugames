// filename: GameState.cs
using UnityEngine;
using System.Collections;

/// <summary>
/// Absract class for the controller of the State Patern.
/// Taken from Smith's and Queiroz's Unity 4.x Cookbook, p. 302.
/// </summary>
public abstract class GameState : MonoBehaviour
{

    protected GameManager gameManager;
    protected void Awake()
    {
        gameManager = GetComponent<GameManager>();
    }

    /// <summary>
    /// This Method is called when the state is entered.
    /// </summary>
    public abstract void OnStateEntered();

    /// <summary>
    /// This Mathod is called when the state is exited.
    /// </summary>
    public abstract void OnStateQuit();

    /// <summary>
    /// This Mathod is called as long as the state doesn't change.
    /// </summary>
    public abstract void StateUpdate();

}
