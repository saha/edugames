// filename: StateWaitingForNextRound.cs
using UnityEngine;
using System.Collections;

/// <summary>
/// This state will be entered after the StateAfterRound state. But ONLY IF the player DIDN'T reach a new level
/// In this state a countdown is displayed and the current level is realoaded.
/// This state is simliar to the other "StateWaitingFor" states.
/// </summary>
public class StateWaitingForNextRound : GameState {

    // references to other scripts.
    public NumberSpawnScript numberSpawnScript;
    public CountDownScript countDownScript;
    public GameEngine gameEngine;
    public LevelXMLParser levelXMLParser;

    /// <summary>
    /// Init.
    /// </summary>
    private void Start()
    {
        // get the components from the GameObjects and save them.
        numberSpawnScript = numberSpawnScript.GetComponent<NumberSpawnScript>();
        countDownScript = countDownScript.GetComponent<CountDownScript>();
        gameEngine = gameEngine.GetComponent<GameEngine>();
        levelXMLParser = levelXMLParser.GetComponent<LevelXMLParser>();
    }

    /// <summary>
    /// Implementation of the OnStateEntered Method.
    /// </summary>
    public override void OnStateEntered()
    {
        // start the countdown
        countDownScript.StartCountDown(GameConfig.Time_After_Round);
        // reload the level
        levelXMLParser.LoadLevel(GameData.Level);
        // update the slider
        numberSpawnScript.UpdateSliderRangeAndPositionMap();
    }

    /// <summary>
    /// Implementation of the OnStateQuit Method.
    /// </summary>
    public override void OnStateQuit()
    {
        // close the countdown UI
        countDownScript.CloseCountdownMenu();
        // start the next round.
        gameEngine.StartRound();
    }

    /// <summary>
    /// Implementation of the StateUpdate Method.
    /// </summary>
    public override void StateUpdate()
    {
        countDownScript.UpdateCountDown();
    }
}
