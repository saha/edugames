// filename: StateWaitingForNextLevel.cs
using UnityEngine;
using System.Collections;

/// <summary>
/// This state will be entered after the StateAfterRound state. But ONLY IF the player reached a new level.
/// In this state the next level is loaded. After a countdown the playing state will be entered.
/// This state is simliar to the other "StateWaitingFor" states.
/// </summary>
public class StateWaitingForNextLevel : GameState {

    // references to other scripts.
    public GameEngine gameEngine;
    public CountDownScript countDownScript;
    public NumberSpawnScript numberSpawnScript;
    public NextLevelScript nextLevelScript;
    public LevelXMLParser levelXMLParser;

    /// <summary>
    /// Init
    /// </summary>
    private void Start()
    {
        // get the components of the gameobjects and save them.
        gameEngine = gameEngine.GetComponent<GameEngine>();
        countDownScript = countDownScript.GetComponent<CountDownScript>();
        numberSpawnScript = numberSpawnScript.GetComponent<NumberSpawnScript>();
        nextLevelScript = nextLevelScript.GetComponent<NextLevelScript>();
        levelXMLParser = levelXMLParser.GetComponent<LevelXMLParser>();
    }

    /// <summary>
    /// Implementation of the OnStateEntered Method.
    /// </summary>
    public override void OnStateEntered()
    {
        // Call for animation start.
        nextLevelScript.StartNextLevelAnimation(GameConfig.Time_Ater_Level);
        // Start the count down.
        countDownScript.StartCountDown(GameConfig.Time_Ater_Level);
        // load the next level.
        levelXMLParser.LoadLevel(GameData.Level);
        // Update the slider range, depending on the new level.
        numberSpawnScript.UpdateSliderRangeAndPositionMap();
    }

    /// <summary>
    /// Implementation of the OnStateQuit Method.
    /// </summary>
    public override void OnStateQuit()
    {
        // Clean uo
        countDownScript.CloseCountdownMenu();
        nextLevelScript.CleanUp();
        // start the round.
        gameEngine.StartRound();
    }

    /// <summary>
    /// Implementation of the StateUpdate Method.
    /// </summary>
    public override void StateUpdate()
    {
        // countdown update.
        countDownScript.UpdateCountDown();
        nextLevelScript.UpdateNextLevelCountDown();
    }
}
