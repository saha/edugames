// filename: StateWaitingForStart.cs
using UnityEngine;
using System.Collections;

/// <summary>
/// This state is ONLY entered after the "StateInput".
/// The only differences between this state and the "StateWaitingForNextRound" are the countdown time and that the level is not reloaded.
/// This state is simliar to the other "StateWaitingFor" states.
/// </summary>
public class StateWaitingForStart : GameState {

    // references to other scripts.
    public GameEngine gameEngine;
    public CountDownScript countDownScript;
    public NumberSpawnScript numberSpawnScript;

    /// <summary>
    /// Init.
    /// </summary>
	private void Start () 
    {
        // get the components from the GameObjects and save them.
        gameEngine = gameEngine.GetComponent<GameEngine>();
        countDownScript = countDownScript.GetComponent<CountDownScript>();
        numberSpawnScript = numberSpawnScript.GetComponent<NumberSpawnScript>();
	}

    /// <summary>
    /// Implementation of the OnStateEntered Method.
    /// </summary>
    public override void OnStateEntered()
    {
        // start countdown.
        countDownScript.StartCountDown(GameConfig.Time_After_Input);
        // update the slider.
        numberSpawnScript.UpdateSliderRangeAndPositionMap();
    }

    /// <summary>
    /// Implementation of the OnStateQuit Method.
    /// </summary>
    public override void OnStateQuit()
    {
        // close countdown UI.
        countDownScript.CloseCountdownMenu();
        // start a new round.
        gameEngine.StartRound();
    }

    /// <summary>
    /// Implementation of the StateUpdate Method.
    /// </summary>
	public override void StateUpdate()
    {
        countDownScript.UpdateCountDown();
    }
	
}