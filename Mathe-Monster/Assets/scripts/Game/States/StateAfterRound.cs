// filename: StateAfterRound.cs
using UnityEngine;
using System.Collections;

/// <summary>
/// This state is entered as soon as every falling number collided with the bottom.
/// In this state the win conditions are checked, points/level incremented and the animation is played.
/// </summary>
public class StateAfterRound : GameState {

    // ref to the gameEngine script
    public GameEngine gameEngine;
    // ref to the gameMenu script
    public GameMenuScript gameMenuScript;

    /// <summary>
    /// Init
    /// </summary>
    private void Start()
    {
        gameEngine = gameEngine.GetComponent<GameEngine>();
        gameMenuScript = gameMenuScript.GetComponent<GameMenuScript>();

    }

    /// <summary>
    /// Implementation of the abstract OnStateEntered method
    /// </summary>
    public override void OnStateEntered()
    {
        gameEngine.CheckNumberHit();
    }

    /// <summary>
    /// Implementation of the abstract OnStateQuit method
    /// </summary>
    public override void OnStateQuit()
    {
        gameMenuScript.UpdateScoreLabels();
    }

    /// <summary>
    /// Implementation of the abstract StateUpdate method
    /// </summary>
    public override void StateUpdate(){}
}
