// filename: StatePlaying.cs
using UnityEngine;
using System.Collections;

/// <summary>
/// This state is entered after each "StateWaitingFor" state.
/// In this state the game is played.
/// </summary>
public class StatePlaying : GameState {

    // reference to the numberSpawnScript.
    public NumberSpawnScript numberSpawnScript;

    /// <summary>
    /// Init
    /// </summary>
    private void Start()
    {
        // get the compnentes and save them,
        numberSpawnScript = numberSpawnScript.GetComponent<NumberSpawnScript>();
    }

    /// <summary>
    /// Implementataion of the abstract OnStateEntered method
    /// </summary>
    public override void OnStateEntered()
    {
        // starts the falling numbers
        numberSpawnScript.StartNumberRain();
    }

    /// <summary>
    /// Implementataion of the abstract OnStateQuit
    /// </summary>
    public override void OnStateQuit()
    {
        // Clean up.
        numberSpawnScript.ClearAndDestroyNumbers();
    }

    /// <summary>
    /// Implementataion of the abstract StateUpdate
    /// </summary>
    public override void StateUpdate()
    {
        // call update function of number spawn script.
        numberSpawnScript.UpdateRoundInProgress();
    }
}
