// file: NumberSpawnScript.cs
using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// This script controlls the numbers which are falling down during a round.
/// You could also see this script as an outsourcing of the stateRoundInProgress class.
/// </summary>
public class NumberSpawnScript : MonoBehaviour
{
    // reference to the game manager
    public GameManager gameManager;

    // the slider 
    public Slider slider;
    // the game field
    public Canvas gameField;
    // the height on which the numbers are spawning.
    public RectTransform spawnHeight;

    // min number range
    public Text minRange;
    // max number range
    public Text maxRange;

    // calculate the position in the scene for every number from minRange to maxRange.
    private Dictionary<int, float> valuePositionMap;
    // a list of the falling numbers.
    private List<GameObject> numbers;


    /// <summary>
    /// Init
    /// </summary>
    void Start()
    {
        // Get the components and save them.
        spawnHeight = spawnHeight.GetComponent<RectTransform>();
        minRange = minRange.GetComponent<Text>();
        maxRange = maxRange.GetComponent<Text>();
        gameField = gameField.GetComponent<Canvas>();
        slider = slider.GetComponent<Slider>();
        gameManager = gameManager.GetComponent<GameManager>();

        // create the objects.
        valuePositionMap = new Dictionary<int, float>();
        numbers = new List<GameObject>();

    }

    /// <summary>
    /// Updates the number range labels in the UI and fills <c>valuePositionMap</c>.
    /// </summary>
    public void UpdateSliderRangeAndPositionMap()
    {
        // clear the old positions.
        valuePositionMap.Clear();
        // set the new slider values
        int start = LevelConfig.Min_Slider_Value;
        int end = LevelConfig.Max_Slider_Value;
        slider.minValue = start;
        slider.maxValue = end;

        // remember the current position/value of the slider.
        float temp = slider.value;

        // set the slider to min vlaue.
        slider.value = LevelConfig.Min_Slider_Value;

        // go from start to end.
        while (start < end)
        {
            // save the current number and the horizontal position of the slider.
            valuePositionMap.Add(start, slider.handleRect.position.x);
            // increment the slider value.
            slider.value++;
            // increment the "number".
            start++;
        }
        // set the slider to its original position/value.
        slider.value = temp;

        // update the labels
        minRange.text = LevelConfig.Min_Slider_Value.ToString();
        maxRange.text = LevelConfig.Max_Slider_Value.ToString();
    }

    /// <summary>
    /// Method is called to trigger the numbers which are falling down.
    /// </summary>
    public void StartNumberRain()
    {
        int[] spawn = new int[LevelConfig.Amount_of_Falling_Numbers];
        Utils.PopulateArray<int>(spawn, 0);

        for (int i = 0; i < spawn.Length; i++)
        {
            spawn[i] = UnityEngine.Random.Range(LevelConfig.Min_Slider_Value, LevelConfig.Max_Slider_Value);
            spawnNumber(spawn[i]);
        }
    }

    /// <summary>
    /// This function spawns falling numbers.
    /// </summary>
    /// <param name="number"></param>
    private void spawnNumber(int number)
    {
        // This is a demonstration on how to create GameObjects via script and add/edit Components.
        // A better way to do this would be using prefabs instead. So don't do this at home!

        //first the Object itself and its transform component
        GameObject obj = new GameObject(number.ToString());
        float xPos;
        valuePositionMap.TryGetValue(number, out xPos);
        obj.transform.position = new Vector3(xPos, spawnHeight.position.y + UnityEngine.Random.Range(0, 50), 0);
        // A normal gameobject just uses tranform. We use Unity.UI/Canvas so we need to specificly add at RectTransform component.
        obj.AddComponent<RectTransform>();
        RectTransform rt = obj.GetComponent<RectTransform>();
        rt.sizeDelta = new Vector2(70, 33); 
        
        // Add the Text and set attributes
        obj.AddComponent<Text>();
        Text text = obj.GetComponent<Text>();
        text.font = (Font)Resources.GetBuiltinResource(typeof(Font), "Arial.ttf");
        text.text = number.ToString();
        text.fontSize = 28;
        text.alignment = TextAnchor.MiddleCenter;
        text.horizontalOverflow = HorizontalWrapMode.Wrap;
        text.verticalOverflow = VerticalWrapMode.Truncate;
        text.color = Color.black;
        text.resizeTextForBestFit = false;
        text.supportRichText = false;

        // Collider, so we can use a collision Event in the RemoveNumberScript
        obj.AddComponent<BoxCollider>();
        BoxCollider col = obj.GetComponent<BoxCollider>();
        col.size = new Vector3(text.rectTransform.rect.width, text.rectTransform.rect.height, 1.0f);

        //RigidBody for the Physics -> Gravity. The Number will be falling  after all.
        obj.AddComponent<Rigidbody>();
        Rigidbody rb = obj.GetComponent<Rigidbody>();
        obj.transform.SetParent(gameField.transform, true);
        obj.transform.localScale = new Vector3(1,1,1);
        rb.constraints = RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationY | RigidbodyConstraints.FreezePositionX | RigidbodyConstraints.FreezePositionZ;
        rb.useGravity = true;
        rb.angularDrag = 0;
        rb.mass = 1;

        // Set the falling speed
        rb.drag = UnityEngine.Random.Range(LevelConfig.Min_Drag, LevelConfig.Max_Drag) * .001f;
        rb.velocity = Vector3.down * UnityEngine.Random.Range(LevelConfig.Min_Fall_Speed, LevelConfig.Max_Fall_Speed);
        numbers.Add(obj);
    }

    /// <summary>
    /// Clean up Function.
    /// </summary>
    /// <returns></returns>
    public int ClearAndDestroyNumbers()
    {
        // Remove and Destroy every object from numbers.
        foreach (GameObject obj in numbers)
        {
            Destroy(obj);
        }
        int cnt = numbers.Count;
        numbers.Clear();
        return cnt;
    }

    /// <summary>
    /// Public access point for other scripts.
    /// </summary>
    /// <param name="obj"></param>
    /// <returns></returns>
    public bool RemoveNumber(GameObject obj)
    {
        return numbers.Remove(obj);
    }

    /// <summary>
    /// Called by the GameManager in the Update Function
    /// </summary>
    public void UpdateRoundInProgress()
    {
        // if every falling number collides with out BottomGameObject the variable numbersAlive has to hit 0. Else something went teribly wrong.
        if (numbers.Count == 0)
        {
            gameManager.NewGameState(gameManager.stateAfterRound);
        }
    }

}