// filename: GameEngine.cs
using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;

/// <summary>
/// This script controlls the slider and checks if the player won the round.
/// </summary>
public class GameEngine : MonoBehaviour
{
    // reference to the gameManager
    public GameManager gameManager;
    // A Text object in which the problem is displayed, which the player has to solve.
    public Text problem;
    // The slider.
    public Slider slider;
    
    // the problem the player has to solve and the solution. 
    public static Pair<string, int> problemAndResult = new Pair<string, int>("0", 0);

   /// <summary>
   /// Init.
   /// </summary>
    private void Start()
    {
        // get the components from the GameObjects and save them.
        gameManager = gameManager.GetComponent<GameManager>();
        problem = problem.GetComponent<Text>();
        slider = slider.GetComponent<Slider>();

        UpdateSliderRange();
    }

    /// <summary>
    /// Starts a new round.
    /// </summary>
    public void StartRound()
    {
        Debug.Log("EquationCount: " + LevelConfig.Equations.Count);
        // Check if there are equations to use.
        if (LevelConfig.Equations.Count != 0)
        {
            // get a random eqaution
            problemAndResult = LevelConfig.Equations[UnityEngine.Random.Range(0, LevelConfig.Equations.Count - 1)];
        }
        else // if not use a random number from minSliderVal - maxSliderVal
        {   
            // create a random number
            int randomNumber = UnityEngine.Random.Range (LevelConfig.Min_Slider_Value, LevelConfig.Max_Slider_Value);
            // set the number as problem and result.
            problemAndResult.First = randomNumber.ToString();
            problemAndResult.Second = randomNumber;
        }
        // update the ui.
        problem.text = problemAndResult.First;
    }


    /// <summary>
    /// Updates the range of the slider.
    /// </summary>
    public void UpdateSliderRange()
    {
        // set Slider values.
        slider.minValue = LevelConfig.Min_Slider_Value;
        slider.maxValue = LevelConfig.Max_Slider_Value;
    }

    /// <summary>
    /// Sets the problem und result to 0
    /// </summary>
    public void ResetRequestedNumber()
    {
        problemAndResult.First = "0";
        problemAndResult.Second = 0;

        problem.text = problemAndResult.First;
    }

    /// <summary>
    ///  Is called after a round ends.
    ///  This Function checks if the player has won the round and increments/decrements points and levels.
    ///  This Function also calls the state changes.
    /// </summary>
    public void CheckNumberHit()
    {
        Debug.Log("CheckNumberHit: " + problemAndResult.Second + ", " + slider.value);
        // check if the player has the right number.
        if (problemAndResult.Second == slider.value) // right number!
        {
            // increment points
            GameData.Points++;
            // increment progression
            GameData.LevelProgression++;

            // check if the player has enough progression to go to the next level
            if (GameData.LevelProgression >= LevelConfig.Points_For_Next_Level) // enough progression
            {
                // increment level
                GameData.Level++;
                // reset progression
                GameData.LevelProgression = 0;
                // enter new game state
                gameManager.NewGameState(gameManager.stateWaitingForNextLevel);
                return;
            }
        }
        else // wrong answer
        {
            // decrement progression
            GameData.LevelProgression--;
            // we are fair, so there is no negative progression.
            Math.Max(GameData.LevelProgression, 0);
        }
        // enter new game state
        gameManager.NewGameState(gameManager.stateWaitingForNextRound);
    }

}
