// filename: GameMenuScript.cs
using UnityEngine;
using UnityEngine.UI;
using System.Collections;

/// <summary>
/// This script controls various UI elemts of the game menu.
/// </summary>
public class GameMenuScript : MonoBehaviour
{

    // reference to the gameManamger
    public GameManager gameManager;

    // reference to the background picture of the panel in which the problem is displayed.
    public GameObject airShipPicture;

    // ui elememt which displays the level
    public Text level;
    // ui elememt which displays the points
    public Text points;

    // the slider
    public Slider slider;
    // back to menu button
    public Button backButton;
    // back to input menu button
    public Button inputButton;

    /// <summary>
    /// Init
    /// </summary>
    void Start()
    {
        // get the components from the GameObjects and save them.
        gameManager = gameManager.GetComponent<GameManager>();
        slider = slider.GetComponent<Slider>();
        backButton = backButton.GetComponent<Button>();
        inputButton = inputButton.GetComponent<Button>();
        level = level.GetComponent<Text>();
        points = points.GetComponent<Text>();
        slider = slider.GetComponent<Slider>();

        // set slider values.
        slider.minValue = LevelConfig.Min_Slider_Value;
        slider.maxValue = LevelConfig.Max_Slider_Value;

    }

    /// <summary>
    /// Function for the Back to main menu Button
    /// </summary>
    public void BackToMenuPressed()
    {
        // save
        GameData.Save();
        // back to level 0. (Mainmenu)
        Application.LoadLevel(0);
    }

    /// <summary>
    /// Function for the Back to input menu button
    /// </summary>
    public void BackToInputPressed()
    {
        // save
        GameData.Save();
        // reset the rotation of the picture. In case the button was pressed during an animation.
        airShipPicture.transform.rotation = new Quaternion(0, 0, 0, 0);
        // enter new game state
        gameManager.NewGameState(gameManager.stateInput);
    }

    /// <summary>
    /// Sets the Buttons/Silder of the UI to interactable
    /// </summary>
    public void EnableButtons()
    {
        backButton.interactable = true;
        inputButton.interactable = true;
        slider.interactable = true;
    }

    /// <summary>
    /// Sets the Buttons/Silder of the UI to NOT interactable
    /// </summary>
    public void DisableButtons()
    {
        backButton.interactable = false;
        inputButton.interactable = false;
        slider.interactable = false;
    }

    /// <summary>
    /// Updates the level and points UI elements.
    /// </summary>
    public void UpdateScoreLabels()
    {
        level.text = GameData.Level.ToString();
        points.text = GameData.Points.ToString();
    }

    /// <summary>
    /// Unity Update Function
    /// Check for key inputs.
    /// </summary>
    void Update()
    {
        if (Input.GetKeyUp(KeyCode.Escape))
        {
            GameData.Save();
            Application.LoadLevel(0);
        }

        if (Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.LeftArrow))
            slider.value--;
        if (Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.RightArrow))
            slider.value++;
    }
}
