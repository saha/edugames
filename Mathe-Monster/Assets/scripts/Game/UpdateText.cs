// filename: UpdateText.cs
using UnityEngine;
using UnityEngine.UI;
using System.Collections;

/// <summary>
/// Simple script to update the text of the slider. 
/// </summary>
public class UpdateText : MonoBehaviour {

    /// <summary>
    /// Reference of the Text UI object
    /// </summary>
    public Text text;

    /// <summary>
    /// Updates the Text of the handle
    /// </summary>
    /// <param name="f"></param>
    public void ChangeText(float f)
    {
        text.text = Mathf.CeilToInt(f).ToString();
    }

}
