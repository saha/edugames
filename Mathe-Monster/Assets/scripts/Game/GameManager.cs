// filename: GameManager.cs
using UnityEngine;
using System.Collections;


/// <summary>
/// This Class manages the state of the game.
/// Taken from Smith's and Queiroz's Unity 4.x cookbook, p. 303.
/// </summary>
public class GameManager : MonoBehaviour
{
    /// <summary>
    /// The possible states of the game
    /// </summary>

    public StateInput stateInput;
    public StateWaitingForStart stateWaitingForStart;
    public StatePlaying stateRoundInProgress;
    public StateWaitingForNextRound stateWaitingForNextRound;
    public StateWaitingForNextLevel stateWaitingForNextLevel;
    public StateAfterRound stateAfterRound;

    // the current state of the game
    private GameState currentState;

    /// <summary>
    /// Init - Before Start
    /// </summary>
    private void Awake()
    {
        // Get the Componets from the GameObjects and save them.
        stateInput = GetComponent<StateInput>();
        stateWaitingForStart = GetComponent<StateWaitingForStart>();
        stateRoundInProgress = GetComponent<StatePlaying>();
        stateWaitingForNextRound = GetComponent<StateWaitingForNextRound>();
        stateWaitingForNextLevel = GetComponent<StateWaitingForNextLevel>();
        stateAfterRound = GetComponent<StateAfterRound>();
    }

    /// <summary>
    /// Init - After Awake
    /// </summary>
    private void Start()
    {
        // enter the inputstate
        NewGameState( stateInput );
    }

    /// <summary>
    /// Calls the Update function of the <c>currentState</c>.
    /// </summary>
    private void Update()
    {
        if (currentState != null)
            currentState.StateUpdate();

    }

    /// <summary>
    /// Use this Function to enter a new state.
    /// 
    /// gameManager.NewGameState(gameManager.<GameState>);
    /// </summary>
    /// <param name="newState"></param>
    public void NewGameState(GameState newState)
    {
        Debug.Log("New Game State: " + newState);
        if (null != currentState)
            currentState.OnStateQuit();

        currentState = newState;
        currentState.OnStateEntered();
    }
}
