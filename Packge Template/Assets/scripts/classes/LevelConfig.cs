// filename: LevelConfig.cs

using UnityEngine;
using System.IO;
using System.Collections.Generic;

/// <summary>
/// A static class containing all relevant information about a level.
/// </summary>
public static class LevelConfig
{
    // the name of the file.
    // This is an extra variable, because Unity Editor and built Unity Applications use a differnt Path system.
    // Example: 
    // In Editor: Application.dataPath = Assets Folder
    // Built Windows Player:  Application.dataPath = ProjectName_data
    // Also Resources.Load() doesn't want file extentions. 
    public static string Filename = "Level";

    // Path to the directory containing the level files.
    public static string Levels_Directoy = Application.dataPath + Path.DirectorySeparatorChar + "Resources" + Path.DirectorySeparatorChar + "Levels" + Path.DirectorySeparatorChar; // windows_Data/Resources/Levels/
    public static string Path_And_Filename = Levels_Directoy + Filename;  // windows_Data/Resources/Levels/Level

    // Add here you level configuration variables.

    //--------------------------------------------------------------------------
    // There is no Method to Load a level. You need to implement it yourself.
    // There were no parameter which were the same in every level.
    // In some games a level configurations can be implemented a simple txt file and txt reader. 
    // In other games you need xml and own xml parser.
    //--------------------------------------------------------------------------
}
