// filename: StatePlaying.cs
using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;


/// <summary>
/// Template for a state that is active when the game is played.
/// </summary>
public class StatePlaying : GameState
{

    /// <summary>
    /// Init
    /// </summary>
	private void Start () {	}

    /// <summary>
    /// Implementation of the OnStateEntered Method.
    /// </summary>
    public override void OnStateEntered()
    {
       // Start the game
        // Load the level
    }

    /// <summary>
    /// Implementation of the OnStateQuit Method.
    /// </summary>
    public override void OnStateQuit() 
    { 
        // Clean up / Pause or save
    }

    /// <summary>
    /// Implementation of the StateUpdate Method.
    /// </summary>
    public override void StateUpdate() 
    {
        // Some quality of Life hotkey.
        // On Escape Go to the Inputmenu
        if (Input.GetKeyDown(KeyCode.Escape))
            gameManager.NewGameState(gameManager.stateInput);
    }
}
