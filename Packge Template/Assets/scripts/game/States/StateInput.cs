// filename: StateInput.cs
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;


/// <summary>
/// This script contains the functions for the Input menu and controlls the flow of it.
/// 
/// <remarks>Depending on your game and/or data you want to save you need to adjust this class and the GameData Class.</remarks>
/// </summary>
    public class StateInput : GameState
    {
        // the Input menu
        public Canvas inputMenu;
        public InputField firstNameField;
        public InputField lastNameField;

        // This objects are optional you dont need to use them, but they are there if you want do display the current points, level and player name somewhere.
        [Header("Optional")]
        public Text textPoints;
        public Text textLevel;
        public Text playerName;
       

        /// <summary>
        /// Init
        /// </summary>
        private void Start()
        {
            // Get Components from the Object and save them.
            inputMenu = inputMenu.GetComponent<Canvas>();
            firstNameField = firstNameField.GetComponent<InputField>();
            lastNameField = lastNameField.GetComponent<InputField>();

            if (textLevel)
                textLevel = textLevel.GetComponent<Text>();
            if (textPoints)
                textPoints = textPoints.GetComponent<Text>();
        }

        /// <summary>
        /// Implementation of the OnStateEntered Method.
        /// </summary>
        public override void OnStateEntered()
        {
            // Make the inputmenu visible
            ShowInputMenu();

            // If there is allready a name in those static variables, then that means, that the player returned into the Inputmenu during the game.
            // Here is a good point to save the GameData.
            if (GameData.FirstName.Length > 0 && GameData.LastName.Length > 0)
            {
                SessionTimer.StopSessionTimer();
                GameData.Save();
            }
        }

        /// <summary>
        /// Implementation of the OnStateQuit Method.
        /// </summary>
        public override void OnStateQuit()
        {
            // Make sure that there is somethign written in the InputFields.
            if (firstNameField.text.Length > 0 && lastNameField.text.Length > 0)
            {
                // Save the names in a Static Variable.
                GameData.FirstName = firstNameField.text;
                GameData.LastName = lastNameField.text;
            }
            // Try to Load that from that User.
            GameData.Load();

            // Put the name into the UI. This won't cause an error even if there is no Field on the UI.
            UpdatePlayerNameText();

            //Put the Loaded Points and Level into the UI. This won't cause an error even if there are no fields on the UI.
            GameData.UpdatePointLable(textPoints, false);
            GameData.UpdateLevelLable(textLevel, false);

            // Make the inputmenu invisble.
            HideInputMenu();
            // start a timer.
            SessionTimer.StartSessionTimer();
        }

        /// <summary>
        /// Implementation of the StateUpdate Method.
        /// </summary>
        public override void StateUpdate() 
        {
            // Some quality of like hotkeys

            // "Return" or enter is the same as pressing the "OK" Button
            if (Input.GetKeyDown(KeyCode.Return))
                Button_OKPressed();

            // "Escape" or Esc is the same as pressing the "cancle" Button.
            if(Input.GetKeyDown(KeyCode.Escape) )
                Button_CancledPressed();
        
        }

        /// <summary>
        /// Function for the "OK" Button.
        /// </summary>
        public void Button_OKPressed()
        {
            // Make sure there is something written in the inputfields
            if(firstNameField.text.Length > 0 && lastNameField.text.Length > 0)

            //enter a new state.
            gameManager.NewGameState(gameManager.statePlaying);
        }

        /// <summary>
        /// Function for the "Cancle" Button.
        /// </summary>
        public void Button_CancledPressed()
        {
            // Return to the mainmenu.
            // You might need to change this if your MainMenu is not Level 0. 
            // This can be checked in the Build Settings.
            Application.LoadLevel(0);
        }

        /// <summary>
        /// Function to update the static variabe for the lastname.
        /// Used for onValueChanged event for LastNameInputField.
        /// </summary>
        /// <param name="t"></param>
        public void InputField_LastName_Changed(string t)
        {
            GameData.LastName = t;
        }

        /// <summary>
        /// Function to update the static variabe for the firstname.
        /// Used for onValueChanged event for LastNameInputField.
        /// </summary>
        /// <param name="t"></param>
        public void InputField_FirstName_Changed(string t)
        {
            GameData.FirstName = t;
        }

        /// <summary>
        /// Function to set Player name into the optional <c>playerName</c>.
        /// </summary>
        private void UpdatePlayerNameText()
        {
            if(playerName)
            playerName.text = GameData.FirstName + " " + GameData.LastName;
        }

        /// <summary>
        /// Activats the Inputmenu and insters First and Lastname if there are some.
        /// </summary>
        private void ShowInputMenu()
        {
            if (GameData.FirstName.Length > 0)
                firstNameField.text = GameData.FirstName;

            if (GameData.LastName.Length > 0)
                lastNameField.text = GameData.LastName;

            inputMenu.enabled = true;
        }

        /// <summary>
        /// Disables the inputmenu.
        /// </summary>
        private void HideInputMenu()
        {
            inputMenu.enabled = false;
        }
    }