// filename StateAfterRound.cs
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// This state can be used to do level transitions, play animations or it can be not used at all.
/// </summary>
public class StateAfterRound : GameState
{

    /// <summary>
    /// Init
    /// </summary>
    private void Start(){}

    /// <summary>
    /// Implementation of the abstract OnStateEntered method
    /// </summary>
    public override void OnStateEntered()
    {

    }

    /// <summary>
    /// Implementation of the abstract OnStateQuit method
    /// </summary>
    public override void OnStateQuit()
    {
       
    }

    /// <summary>
    /// Implementation of the abstract StateUpdate method
    /// </summary>
    public override void StateUpdate()
    {
      
    }
}
