// filename: GameExitButtonScript.cs
using UnityEngine;
using UnityEngine.UI;
using System.Collections;

/// <summary>
/// This script contains the functions for the Exit Button and the "Are you sure that you want to exit" Buttons.
/// </summary>
public class GameExitButtonScript : MonoBehaviour {

    // The "Are you sure?" prompt
    public Canvas exitMenu;

    // Game Start Button. This is used to deactive its interactablity during the "Are you sure?" promt.
    public Button startButton;

    // The Exit Button
    public Button exitButton;

    /// <summary>
    /// Init
    /// </summary>
    private void Start()
    {
        // Get the componentes from the GameObjects and save them. 
        // It is more efficent to do this in the Start Method then during other times, since frequent GetComopnent Calls can be expansive.
        exitButton = exitButton.GetComponent<Button>();
        startButton = startButton.GetComponent<Button>();
        exitMenu = exitMenu.GetComponent<Canvas>();

        // Make the "Are you sure?" promt invisible.
        exitMenu.enabled = false;
    }

    /// <summary>
    /// Function for the Game ExitButton
    /// </summary>
    public void GameExitPressed()
    {
        exitMenu.enabled = true;
        exitButton.interactable = false;
        startButton.interactable = false;
    }

    /// <summary>
    /// Funtion for the "Are you sure?" promt Yes Button.
    /// </summary>
    public void YesPressed()
    {
        Application.Quit();
    }

    /// <summary>
    /// Funtion for the "Are you sure?" promt No Button.
    /// </summary>
    public void NoPressed()
    {
        exitMenu.enabled = false;
        exitButton.interactable = true;
        startButton.interactable = true;
    }
	
}
