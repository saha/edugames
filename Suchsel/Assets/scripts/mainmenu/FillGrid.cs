using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class FillGrid : MonoBehaviour {

    public int slots = 42;
    public GameObject prefab;

    private GridLayoutGroup gridLayout;
	// Use this for initialization
	private void Start () 
    {
        gridLayout = GetComponent<GridLayoutGroup>();
        Fill();
	}

    private void Fill()
    {
        for(int i = 0; i<slots; i++)
        {
            GameObject obj = Instantiate(prefab);
            char c = (char)('A' + Random.Range(0, 26));
            obj.name = c.ToString();
            obj.transform.GetChild(0).gameObject.GetComponent<Text>().text = c.ToString();
            obj.transform.SetParent(gridLayout.transform, false);
        }
    }
	
}
