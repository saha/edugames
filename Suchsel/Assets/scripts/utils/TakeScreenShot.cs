using UnityEngine;
using System;
using System.Collections;
using System.IO;

public class TakeScreenShot : MonoBehaviour {

    public string prefix = "Screenshot";
    public string screenShotDir;
    public int scale = 1;
    public bool useReadPixels = false;
    private Texture2D texture;

    private void Start()
    {
        screenShotDir = Path.GetDirectoryName(Application.dataPath) + Path.DirectorySeparatorChar + "Screenshots";
    }

	// Update is called once per frame
	private void Update () {
        if (Input.GetKeyDown(KeyCode.P))
            StartCoroutine(TakeShot());
	}

    private IEnumerator TakeShot()
    {
        if (!Directory.Exists(screenShotDir))
            Directory.CreateDirectory(screenShotDir);

        string date = System.DateTime.Now.ToString("_d.MMM.yyyy HH-mm");
        int screenHeight = Screen.height;
        int screenWidth = Screen.width;

        Rect sRect = new Rect(0, 0, screenWidth, screenHeight);

        if (useReadPixels)
        {
            yield return new WaitForEndOfFrame();
            texture = new Texture2D(screenWidth, screenHeight, TextureFormat.RGB24, false);
            texture.ReadPixels(sRect, 0, 0);
            texture.Apply();
            byte[] bytes = texture.EncodeToPNG();
            Destroy(texture);
            File.WriteAllBytes(screenShotDir + Path.DirectorySeparatorChar + prefix + date + ".png", bytes);
        }
        else
        {
            Application.CaptureScreenshot(screenShotDir + Path.DirectorySeparatorChar + prefix + date + ".png", scale);
        }
        Debug.Log("Screenshot taken: " + prefix + date +".png");
    }
}
