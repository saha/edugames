// Filename: Pair.cs

/// <summary>
/// Simple version of the c++ class std::pair
/// </summary>
/// <typeparam name="T"></typeparam>
/// <typeparam name="U"></typeparam>
public class Pair<T, U>
{

    /// <summary>
    /// Default constructor
    /// </summary>
    public Pair()
    {
    }

    /// <summary>
    /// Overloaded constructor
    /// </summary>
    /// <param name="first"></param>
    /// <param name="second"></param>
    public Pair(T first, U second)
    {
        this.First = first;
        this.Second = second;
    }

    /// <summary>
    /// Getter/Setter for the first element
    /// </summary>
    public T First { get; set; }

    /// <summary>
    /// Getter/Setter for the second element
    /// </summary>
    public U Second { get; set; }

    /// <summary>
    /// Overwritten ToString
    /// </summary>
    new public string ToString()
    { return this.First + "," + this.Second; }

}
