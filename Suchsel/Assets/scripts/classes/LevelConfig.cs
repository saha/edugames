// filename: LevelCondig
using UnityEngine;
using System.IO;
using System.Collections.Generic;

/// <summary>
/// A static class containing all relevant information about a level.
/// </summary>
public static class LevelConfig
{
    // the name of the file.
    // This is an extra variable, because Unity Editor and built Unity Applications use a differnt Path system.
    // Example: 
    // In Editor: Application.dataPath = Assets Folder
    // Built Windows Player:  Application.dataPath = ProjectName_data
    // Also Resources.Load() doesn't want file extentions. 
    public static string Filename = "Level";

    // Path to the directory containing the level files.
    public static string Levels_Directoy = Application.dataPath + Path.DirectorySeparatorChar + "Resources" + Path.DirectorySeparatorChar + "Levels" + Path.DirectorySeparatorChar; // windows_Data/Resources/Levels/
    public static string Path_And_Filename = Levels_Directoy + Filename;  // windows_Data/Resources/Levels/Level

    // The size of the grid
    public static int Size;

    // difficulty option. If selected letters were not part of a hidden word they will be highlighted.
    public static bool HighlightWrongLetters;

    // difficulty option. If this is true all letters are converted to lower case. Else they will be inserted as they are written in the level config file.
    public static bool OnlySmallLetters;

    // the content of the gird in form of a list of lists
    public static List<List<GridSlot>> slots;

    // the list of hidden words
    public static List<Entry> Entries;

    // the amount of found letters
    public static int correctLetters;

    // the amount of hidden letters
    public static int neededLetters;
}
