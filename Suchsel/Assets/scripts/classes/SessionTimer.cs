// filename: SessionTimer.cs
using System;

/// <summary>
/// A small static class. Used to stop a time inveral.
/// Usage:
///             SesssionTimer.StartSessionTimer();
///             TimeSpan ts = SessionTimer.StopSessionTimer();
/// </summary>
public static class SessionTimer
{

    private static DateTime Start;

    public static void StartSessionTimer()
    {
        Start = DateTime.Now;
    }

    public static TimeSpan StopSessionTimer()
    {
        return DateTime.Now.Subtract(Start);
    }

}
