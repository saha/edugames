// filename: GameConfig.cs
using System.IO;
using UnityEngine;

/// <summary>
/// A static class containing different game configurations.
/// </summary>
public static class GameConfig
{

    // the Version of the Application
    public static string Application_Version = "1.3";

    // the name of the file.
    // This is an extra variable, because Unity Editor and built Unity Applications use a differnt Path system.
    // Example: 
    // In Editor: Application.dataPath = Assets Folder
    // Built Windows Player:  Application.dataPath = ProjectName_data
    // Also Resources.Load() doesn't want file extentions. 
    public static string Filename = "Config";

    // Path to the config file. Used for external configuration.
    public static string Path_And_Filename = Application.dataPath + Path.DirectorySeparatorChar + "Resources" + Path.DirectorySeparatorChar + Filename + ".xml";
}
