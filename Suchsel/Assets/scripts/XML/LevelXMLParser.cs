// filename: LevelXMLParser.cs
using UnityEngine;
using System.Collections.Generic;
using System;
using System.IO;
using System.Xml;

/// <summary>
/// Used to Read XML files and parse the data into <c><LevelConfig/c>.
/// </summary>
public class LevelXMLParser : ILevelXMLParser {

    // the list of words that are in this level.
    private List<Entry> entries;


    /// <summary>
    /// Implementation of the ILevelXMLParser interface
    /// Reads level <para>level</para> and pareses it into <c>LecvelConfig</c>.
    /// </summary>
    /// <param name="level"></param>
    public override void LoadLevel(int level)
    {
        Debug.Log("Loading Level " + level.ToString());
        if (level >= CountLevels() )
        {
            Debug.Log("Max Level reached.\n Loading previous level: " + (CountLevels()-1));
            level = CountLevels()-1;
            GameData.Level = level;
        }

        string lvl = LevelConfig.Path_And_Filename + "_" + level.ToString() + ".xml"; // .../Resources/Levels/Level_X.xml
      
        string xmlText = Utils.ReadTextFile(lvl);

        ParseAndSetLevelConfigFromXML(xmlText);
    }

    /// <summary>
    /// Parses a xml string into <c>LevelConfig</c>
    /// </summary>
    /// <param name="xmlText"></param>
    private void ParseAndSetLevelConfigFromXML(string xmlText)
    {
        // create an new XML doc.
        XmlDocument xmlDoc = new XmlDocument();
        // convert a string into a XML doc.
        xmlDoc.Load(new StringReader(xmlText));

        // set the first node.
        XmlNode rootNode = xmlDoc.FirstChild;

        LevelConfig.Size = int.Parse(rootNode.FirstChild.Attributes[0].Value); // Second Child
        LevelConfig.HighlightWrongLetters = rootNode.FirstChild.NextSibling.Attributes[0].Value == "true" ? true : false;  // Third Child
        LevelConfig.OnlySmallLetters = rootNode.FirstChild.NextSibling.NextSibling.Attributes[0].Value == "true" ? true : false; // Forth Child

        SetEntries(rootNode.LastChild); // LastChild
    }

    /// <summary>
    /// Parses the entries into <c>LevelConfig</c>
    /// </summary>
    /// <param name="child"></param>
    private void SetEntries(XmlNode child)
    {
        XmlNodeList childNodes = child.ChildNodes;
        List<Entry> entries = new List<Entry>();

        foreach(XmlNode node in childNodes)
        {
            string word = node.Attributes[0].Value;                                                    // word
            Entry.AlignmentType allignment = ParseEnum<Entry.AlignmentType>(node.Attributes[1].Value); // allignment
            int col = int.Parse(node.Attributes[2].Value);                                             // col
            int row = int.Parse(node.Attributes[3].Value);                                             // row
            Entry.DirectionType direction = ParseEnum<Entry.DirectionType>(node.Attributes[4].Value);  // direction

            Entry entry = new Entry(word, allignment, col, row, direction);
            entries.Add(entry);
            Debug.Log("Added: "+ entry.ToString());
        }
        Debug.Log("Added: " + childNodes.Count + " entries in total");
        LevelConfig.Entries = entries;

    }

    /// <summary>
    /// A generic enumparser.
    /// It parses a string into an enum type.
    /// THis is used for alignment and direction
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="value"></param>
    /// <returns></returns>
    private T ParseEnum<T>(string value)
    {
        // The performance of Enum.Parse is according to Stackoverflow not that good, because it is implemented via reflection.
        // Since we pare only a "few" words once in a while, performance isn't an issue.
        return (T)Enum.Parse(typeof(T), value, true);
    }

}
