using UnityEngine;
using System.IO;

/// <summary>
/// A Interface for a xml level parser.
/// </summary>
public abstract class ILevelXMLParser : MonoBehaviour 
{
    /// <summary>
    /// Abstract Method to load a specific level file.
    /// </summary>
    /// <param name="level"></param>
    public abstract void LoadLevel(int level);

    /// <summary>
    /// A Method to cound the level files on the level directory.
    /// It is assumed that every level has its owen file.
    /// </summary>
    /// <returns></returns>
    protected  int CountLevels()
    {
        return Directory.GetFiles(LevelConfig.Levels_Directoy,"Level_*.xml").Length;
    }
}
