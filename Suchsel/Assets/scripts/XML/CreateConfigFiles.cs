// filename: CreateConfigFiles.cs
using UnityEngine;
using System.IO;
using System.Xml;

/// <summary>
/// This script creats  default .xml level files. After the project is built and started for the first time.
/// And recreates them if there aren't any level files.
/// </summary>
public class CreateConfigFiles : MonoBehaviour {

    private string resLevelPath = @"Levels";

    /// <summary>
    /// Init
    /// </summary>
    private void Start()
    {
        CreateDefaultLevelXMLFiles();
    }

    /// <summary>
    /// Creates the default level files.
    /// </summary>
    private void CreateDefaultLevelXMLFiles()
    {
          // Check if the level dir exists, if not create it.
        if (!Directory.Exists(LevelConfig.Levels_Directoy))
            Directory.CreateDirectory(LevelConfig.Levels_Directoy);

        // get all files as text asset from the resources sub dir into an array.
        TextAsset[] txtAssets = Resources.LoadAll<TextAsset>(resLevelPath);
        // a counter
        int cnt = 0;
        // go through evert asset in the array
        foreach( var asset in txtAssets)
        {
             // filename
            string file = LevelConfig.Path_And_Filename + "_" + cnt.ToString() + ".xml";
            // check if the file exists
            if(!File.Exists(file))
            {
                // create a new xml doc.
                XmlDocument doc = new XmlDocument();
                // fill the xml doc
                doc.LoadXml(asset.text);
                // save the xml doc on the hardware.
                doc.Save(file);
            }
            // inc the counter
            cnt++;
        }
    }
}
