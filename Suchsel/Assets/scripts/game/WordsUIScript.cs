// filename: WordsUIScript.cs
using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;


/// <summary>
/// This script controlls the ui which display the hidden words in the grid.
/// </summary>
    public class WordsUIScript : MonoBehaviour
    {
        // the layout in which the words are added.
        public Transform layout;
        // the prefab for the words.
        public Text textPrefab;

        /// <summary>
        /// Init.
        /// </summary>
        private void Start()
        {
            // get the components from the GameObjects and save them.
            textPrefab = textPrefab.GetComponent<Text>();
            layout = layout.transform;
            // hide the UI
            layout.parent.gameObject.SetActive(false);
        }

        /// <summary>
        /// Function for the Button the togggles the visibility of the UI
        /// </summary>
        /// <param name="newState"></param>
        public void Button_Toggle(bool newState)
        {
            if (newState)
            {
                Debug.Log(newState);
                layout.parent.gameObject.SetActive(true);
            }
            else
            {
                Debug.Log(newState);
                layout.parent.gameObject.SetActive(false);
            }
        }

        /// <summary>
        /// Function to add a word to the UI.
        /// </summary>
        public void AddWords()
        {
            foreach (var entry in LevelConfig.Entries)
            {
                Text text = Instantiate(textPrefab);
                text.text = entry.Word;
                text.name = entry.Word;
                text.transform.SetParent(layout, false);
            }
        }

        /// <summary>
        /// Clears the UI.
        /// </summary>
        public void Clear()
        {
            for (int i = 0; i < layout.childCount; i++)
            {
                Destroy(layout.GetChild(i).GetComponent<Text>());
                Destroy(layout.GetChild(i).gameObject);
            }
        }

        /// <summary>
        /// Highlights word <para>word</para>
        /// </summary>
        /// <param name="word"></param>
        public void WordFound(string word)
        {
            Text t; 
            if (layout.FindChild(word))
               t  = layout.FindChild(word).GetComponent<Text>();
            else
                t = layout.FindChild(word.Reverse()).GetComponent<Text>();

            t.color = Utils.UnityGreen;
        }
    }