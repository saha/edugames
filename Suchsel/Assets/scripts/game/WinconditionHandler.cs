// filename: WinconditionHandler.cs
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;


/// <summary>
/// This script checks if the player has completed the round by finding every hidden word.
/// </summary>
    public class WinconditionHandler : MonoBehaviour, IPointerEnterHandler, IPointerDownHandler, IPointerUpHandler
    {
        // reference to the state
        private StatePlaying statePlaying;

        // the currently selected letters
        public static List<GridSlot> itemsSelected;
        // a bool to check if the player is selecting letters.
        private static bool selectionInProgress;

        /// <summary>
        /// Init
        /// </summary>
        private void Start()
        {
            // get the component from the GameObject and save it
            statePlaying = GameObject.Find("GameManager").GetComponent<StatePlaying>();
            // create the list
            itemsSelected = new List<GridSlot>(LevelConfig.Size);
        }

        /// <summary>
        /// Implementation of the IPointerDownHandler interface
        /// This function is triggered as soon as the player presses the mousebutton.
        /// </summary>
        /// <param name="eventData"></param>
        public void OnPointerDown(PointerEventData eventData)
        {
            // we beginn selecting
            selectionInProgress = true;
            // get the slot 
            GridSlot slot = gameObject.GetComponent<GridSlot>();
            // check if that slot is not allready selected.
            if (!slot.Selected)
                AddItemToSelection(gameObject);
        }

        /// <summary>
        /// Implementation of the IPointerEnterHandler interface
        /// This function triggers if the cursor enters the game object.
        /// </summary>
        /// <param name="eventData"></param>
        public void OnPointerEnter(PointerEventData eventData)
        {
            // check if the player is selecting.
            if (selectionInProgress)
            {
                // get the slot
                GridSlot slot = gameObject.GetComponent<GridSlot>();
                // check if this slot is allready selected.
                if (!slot.Selected) // slot was not selected
                {
                    // Add this slot to the selection
                    // Only horizontal and vertiacl selection
                    if (slot.XPos == WinconditionHandler.itemsSelected[0].XPos ||
                        slot.YPos == WinconditionHandler.itemsSelected[0].YPos)
                    {
                        // again check if this slot is for any reason allready in the list.
                        if (!itemsSelected.Contains(slot))
                            AddItemToSelection(gameObject);
                    }
                }
                else // slot was selected
                {
                    // deselect it.
                    RemoveItemFromSelection((itemsSelected[itemsSelected.Count - 1]).gameObject);
                }
            }
        }

        /// <summary>
        /// This function adds a GameObject to the list of selected items.
        /// </summary>
        /// <param name="item"></param>
        private void AddItemToSelection(GameObject item)
        {
            // get the slot of the item
            GridSlot slot = item.GetComponent<GridSlot>();
            // mark it as selected
            slot.Selected = true;
            // highlight it
            item.GetComponent<Image>().color = Utils.UnityRed;
            // add the slot to the list
            itemsSelected.Add(slot);

            // Don't forget this!
            slot.transform.GetChild(0).GetComponent<CanvasGroup>().blocksRaycasts = false;
        }

        /// <summary>
        /// This function removes a GameObject to the list of selected items.
        /// </summary>
        /// <param name="item"></param>
        private void RemoveItemFromSelection(GameObject item)
        {
            // get the slot
            GridSlot slot = item.GetComponent<GridSlot>();
            // remove the highlight.
            item.GetComponent<Image>().color = Utils.UnityBlack;
            // remove ot from the list.
            itemsSelected.Remove(slot);
            // mark it as not selected
            slot.Selected = false;

            // Don't forget this!
            slot.transform.GetChild(0).GetComponent<CanvasGroup>().blocksRaycasts = true;
        }

        /// <summary>
        ///  Implementation of the IPointerUpHandler interface
        ///  This function is triggered as soon as the player releases the mouse button.
        /// </summary>
        /// <param name="eventData"></param>
        public void OnPointerUp(PointerEventData eventData)
        {
            // we end the selection process.
            selectionInProgress = false;
            // to check which letters the player has selected we build a word.
            StringBuilder word = new StringBuilder();
            // go through the list of selected items 
            foreach (var slot in itemsSelected)
            {
                // revert the selection.
                slot.Selected = false;
                slot.gameObject.GetComponent<Image>().color = Utils.UnityBlack;
                slot.transform.GetChild(0).GetComponent<CanvasGroup>().blocksRaycasts = true;

                // get the content of the slot and add it to the string builder.
                word.Append(slot.Letter);
            }

            // Check if the selected word is a hidden word.
            // The words can be written left to right and right to left. That's why we check both ways.
            if (CheckWord(word.ToString()) || CheckWord(word.ToString().Reverse()))
                WordsCorrect(word.ToString()); // word was correct.
            else // word was not correct
            {
                // check if "easy mode"
                if (LevelConfig.HighlightWrongLetters)
                {
                    foreach (var slot in itemsSelected)
                    {
                        if (!(slot.PartOfWords.Count > 0))
                        {
                            // highlight the letters that were not part of any hidden word.
                            slot.gameObject.GetComponent<Image>().color = Utils.UnityRed;
                            slot.Text.color = Utils.UnityBlack;
                        }
                    }
                }
            }
            Debug.Log(LevelConfig.correctLetters + "/" + LevelConfig.neededLetters);
            // clear the list.
            itemsSelected.Clear();
        }

        /// <summary>
        /// Helper function to check is a word is a hidden word.
        /// </summary>
        /// <param name="word"></param>
        /// <returns></returns>
        private bool CheckWord(string word)
        {
            Debug.Log("Checking Word: " + word);
            // go through the list of selected items
            foreach (GridSlot slot in itemsSelected)
            {
                // each slot knows the words it is part of.
                foreach (string s in slot.PartOfWords)
                {
                    if (s.Equals(word))
                        return true;
                }
            }
            return false;
        }

        /// <summary>
        /// This function is triggered if the player seleced a hidden word.
        /// </summary>
        /// <param name="word"></param>
        private void WordsCorrect(string word)
        {
            // go through the list of selected items
            foreach (var slot in itemsSelected)
            {
                // sometimes 2 words are overlapping. In that case a letter might be counted twice.
                // check if this slot needs to be counted
                if (slot.TimesCounted != slot.NecessaryCounts) // it needs to be counted
                {
                    slot.TimesCounted++;
                    // Play a Animation.
                    slot.GetComponent<Animation>().Play();
                }
                else // it doesn't need to be counted
                {
                    itemsSelected.Clear();
                    return;  
                }
            }
            // add the correct letters to the total of the found letters.
            LevelConfig.correctLetters += word.Length;
            // increment the points of the player depending on the level.
            GameData.Points += GameData.Level == 0 ? 1 : GameData.Level;
            // update the UI.
            statePlaying.UpdateUI(word);

            // StatePlaying.neededLetters are the letters of every word, hidden in the grid.
            // StatePlaying.correctLetters are the letters of every word, that were allready found.

            // Check if all letters/words are found.
            if (LevelConfig.correctLetters == LevelConfig.neededLetters)
            {
                Debug.Log("Won!");
                // increment the level.
                GameData.UpdateLevelLable(null, true); // Same as GameData.Level++;
                // call round won function.
                statePlaying.RoundWon();
            }
            
            itemsSelected.Clear();
        }
    }
