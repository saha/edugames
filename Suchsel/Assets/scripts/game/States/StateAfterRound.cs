// filename StateAfterRound.cs
using UnityEngine;
using System.Collections;


/// <summary>
/// In this state a propmt will be displays telling the player that he has found every hidden word.
/// </summary>
    public class StateAfterRound : GameState
    {
        // the UI of the prompt.
        public Canvas winUI;

        /// <summary>
        /// Init.
        /// </summary>
        private void Start()
        {
            // make the UI invisible.
            winUI.gameObject.SetActive(false);
        }

        /// <summary>
        /// Implementation of the abstract OnStateEntered method
        /// </summary>
        public override void OnStateEntered()
        {
            // make the UI visible
            winUI.gameObject.SetActive(true);
        }

        /// <summary>
        /// Implementation of the abstract OnStateQuit method
        /// </summary>
        public override void OnStateQuit()
        {
            // make the UI invisible
            winUI.gameObject.SetActive(false);
        }

        /// <summary>
        /// Implementation of the abstract StateUpdate method
        /// </summary>
        public override void StateUpdate(){}

        /// <summary>
        /// Function for the Button of the UI
        /// </summary>
        public void Button_WinUI()
        {
            // enter the next gamestate.
            gameManager.NewGameState(gameManager.statePlaying);
        }
    }

