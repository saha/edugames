// filename: StatePlaying.cd
using System; 
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


/// <summary>
/// This state controlls the game play of the application.
/// It Create the game field. Loads the level and fills the game field with random letters and the hidden words.
/// </summary>
    public class StatePlaying : GameState
    {
        // the prefab for a slot
        public GridSlot slotPrefab;

        // reference tothe script that controls the UI that shows which words are hidden in the game field.
        public WordsUIScript wordsUIScript;

        // the layout opf the gamefield.
        // every slot prefab will be a child of this.
        public GridLayoutGroup grid;

        // UI element that displays the points
        public Text points;
        // UI element that displays the level
        public Text level;

        // the letter which will be filled in randomly.
        // A user of the programm gave the advise to use small letter instead of capital.
        private char[] alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ".ToLower().ToCharArray();

        // for loading the levels
        private ILevelXMLParser levelXMLParser;

        /// <summary>
        /// Init.
        /// </summary>
        private void Start()
        {
            // get the components from the GameObjects and save them.
            points = points.GetComponent<Text>();
            level = level.GetComponent<Text>();
            levelXMLParser = gameManager.transform.GetChild(0).gameObject.GetComponent<LevelXMLParser>();
            wordsUIScript = wordsUIScript.GetComponent<WordsUIScript>();

            // create a list of lists.
            LevelConfig.slots = new List<List<GridSlot>>();
        }

        /// <summary>
        /// Implementation of the OnStateEntered Method.
        /// </summary>
        public override void OnStateEntered()
        {
            // make the grid visible
            grid.gameObject.SetActive(true);
            // start the round
            StartRound();
            // add the hidden words to the UI.
            wordsUIScript.AddWords();
        }

        /// <summary>
        /// Implementation of the OnStateQuit Method.
        /// </summary>
        public override void OnStateQuit()
        {
            CleanUp();
        }

        /// <summary>
        /// Implementation of the StateUpdate Method.
        /// </summary>
        public override void StateUpdate()
        {
            // Check for Key input.
            if (Input.GetKeyDown(KeyCode.Escape))
                gameManager.NewGameState(gameManager.stateInput); // go to the input menu.
        }

        /// <summary>
        /// public access point to enter a new game state.
        /// </summary>
        public void RoundWon()
        {
            gameManager.NewGameState(gameManager.stateAfterRound);
        }

        /// <summary>
        /// This function inserts the hidden words into the grid.
        /// </summary>
        /// <param name="entries"></param>
        private void InsertWordsIntoGrid(List<Entry> entries)
        {
            // go through every entry
            foreach (Entry entry in entries)
            {
                // check for "hard mode"
                if (LevelConfig.OnlySmallLetters) // hard mode
                    entry.Word = entry.Word.ToLower();

                // We count the LETTERS hidden in the grind to decice if the player has won. We DON'T cont the WORDS.
                LevelConfig.neededLetters += entry.Word.Length; 

                try
                {
                    // check for AlignmentType
                    if (entry.Alignment.Equals(Entry.AlignmentType.horizontal))
                    {
                        // go through every letter of the word
                        for (int i = 0; i < entry.Word.Length; i++)
                        {
                            LevelConfig.slots[entry.Row][entry.Col + i].PartOfWords.Add(entry.Word);
                            // check for DirectionType and insert the letter accordingly
                            if (entry.Direction == Entry.DirectionType.normal)
                                LevelConfig.slots[entry.Row][entry.Col + i].transform.GetChild(0).GetComponent<Text>().text = entry.Word[i].ToString();
                            else
                                LevelConfig.slots[entry.Row][entry.Col + i].transform.GetChild(0).GetComponent<Text>().text = entry.Word.Reverse()[i].ToString();

                        }
                    }
                    else
                    {
                        for (int i = 0; i < entry.Word.Length; i++)
                        {
                             LevelConfig.slots[entry.Row + i][entry.Col].PartOfWords.Add(entry.Word);
                            if (entry.Direction == Entry.DirectionType.normal)
                                LevelConfig.slots[entry.Row + i][entry.Col].transform.GetChild(0).GetComponent<Text>().text = entry.Word[i].ToString();
                            else
                                LevelConfig.slots[entry.Row + i][entry.Col].transform.GetChild(0).GetComponent<Text>().text = entry.Word.Reverse()[i].ToString();                               
                        }
                    }
                }
                catch (Exception e)
                {
                    Debug.LogError("Exception during InsertWordsIntoGrid(): \n" + e.Message.ToString());
                }

                // now we name each GameObject. 
                foreach(var row in LevelConfig.slots)
                {
                    foreach(var item in row)
                    {
                        item.name = item.XPos + ", " + item.YPos + " - " + item.Letter;
                    }
                }
            }

        }

        /// <summary>
        /// This function starts the round
        /// </summary>
        private void StartRound()
        {
            // load the level
            levelXMLParser.LoadLevel(GameData.Level);
            LevelConfig.correctLetters = 0;
            LevelConfig.neededLetters = 0;

            // Prepare the grid
            grid.constraintCount = LevelConfig.Size;
            grid.cellSize = new Vector2(grid.GetComponent<RectTransform>().rect.width / LevelConfig.Size,
                                         grid.GetComponent<RectTransform>().rect.height / LevelConfig.Size);

            // update level label
            GameData.UpdateLevelLable(level, false);

            // call fill grid
            FillGrid();
        }

        /// <summary>
        /// This function fills the grid with slots, adds them to the list and writes random letter from <c>alphabet</c> into the slots.
        /// </summary>
        private void FillGrid()
        {
            // go through each row
            for (int i = 0; i < LevelConfig.Size; i++)
            {
                // go through each col
                List<GridSlot> row = new List<GridSlot>();
                for (int j = 0; j < LevelConfig.Size; j++)
                {
                    // instantiate a slot prefab
                    GridSlot slot = Instantiate<GridSlot>(slotPrefab) as GridSlot;
                    // set the grid as the parent of the slot.
                    slot.transform.SetParent(grid.transform, false);
                    // fill a random letter into the slot
                    slot.transform.GetChild(0).GetComponent<Text>().text = alphabet[UnityEngine.Random.Range(0, alphabet.Length - 1)].ToString();
                    // set the X pos
                    slot.XPos = j;
                    // set the Y pos
                    slot.YPos = i;
                    // create the list and leave it empty
                    slot.PartOfWords = new List<string>();
                    // name the slot.
                    slot.gameObject.name = i + ", " + j;
                    // add the slot to the row
                    row.Add(slot);
                }
                // add the row to the list.
                LevelConfig.slots.Add(row);
            }
            // insert the hidden words into the grid.
            InsertWordsIntoGrid(LevelConfig.Entries);
        }

        /// <summary>
        /// public access point to up the UI that a word was found.
        /// </summary>
        /// <param name="wordFound"></param>
        public void UpdateUI(string wordFound)
        {
            wordsUIScript.WordFound(wordFound);
            GameData.UpdatePointLable(points, false);
          
        }

        /// <summary>
        /// This fucntion was used during prototyping and testing.
        /// It just adds some entries on to a list.
        /// </summary>
        /// <returns></returns>
        [System.Obsolete("Use external .xml Level config files now.")]
        private List<Entry> LoadTestEntries()
        {
            List<Entry> test = new List<Entry>();
            // Do note delete!
            //test.Add(new Entry("Alleine".ToUpper(), Entry.AlignmentType.vertical, 0, 9));
            //test.Add(new Entry("Merkwürdig".ToUpper(), Entry.AlignmentType.vertical, 1, 9, Entry.DirectionType.reverse));
            //test.Add(new Entry("Zusammenkleben".ToUpper(), Entry.AlignmentType.vertical, 2, 0));
            //test.Add(new Entry("kritzeln".ToUpper(), Entry.AlignmentType.vertical, 4, 13));
            //test.Add(new Entry("Schrank".ToUpper(), Entry.AlignmentType.vertical, 6, 14, Entry.DirectionType.reverse));
            //test.Add(new Entry("Dollar".ToUpper(), Entry.AlignmentType.vertical, 8, 14, Entry.DirectionType.reverse));
            //test.Add(new Entry("zerschnippeln".ToUpper(), Entry.AlignmentType.vertical, 10, 9));
            //test.Add(new Entry("anspucken".ToUpper(), Entry.AlignmentType.vertical, 15, 13, Entry.DirectionType.reverse));
            //test.Add(new Entry("Hinten".ToUpper(), Entry.AlignmentType.vertical, 18, 3));
            //test.Add(new Entry("Insel".ToUpper(), Entry.AlignmentType.vertical, 18, 14));
            //test.Add(new Entry("lächeln".ToUpper(), Entry.AlignmentType.vertical, 21, 8));

            //test.Add(new Entry("SCHIEF".ToUpper(), Entry.AlignmentType.horizontal, 15, 4));
            //test.Add(new Entry("Glubschaugen".ToUpper(), Entry.AlignmentType.horizontal, 5, 8, Entry.DirectionType.reverse));

            return test;
        }

        /// <summary>
        /// Clean up function
        /// </summary>
        private void CleanUp()
        {
            // go through all slots
            foreach(var slot in LevelConfig.slots){
                foreach (var item in slot)
                {
                    // destroy the slot and its child.
                    Destroy(item);
                    Destroy(item.gameObject);
                }
            }
            // clear the list
            LevelConfig.slots.Clear();
            // hide the grid
            grid.gameObject.SetActive(false);
            // clear the UI
            wordsUIScript.Clear();
        }
    }


    /// <summary>
    /// Extention of the string datatyp
    /// Thi adds the function "Reverse", which returns the string in reverse directopn. (left to right)
    /// </summary>
    internal static class StringExtensions
    {
        public static string Reverse(this string input)
        {
            char[] arr = input.ToCharArray();
            Array.Reverse(arr);
            return new string(arr);
        }
    }