// filename: ToggleButton.cs
using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;

/// <summary>
/// Implementation of a custom Toggle Button
/// This script is used to control the state of a button.
/// The button looks different depending on the state.
/// </summary>
public class ToggleButton : Toggle {

    Sprite normalSprite;

	/// <summary>
	/// Init
	/// </summary>
	protected override void Start () {
        base.Start();
        // get the sprite(image) of the button
        normalSprite = ((Image)targetGraphic).sprite;
        // add a listener to the button
        onValueChanged.AddListener(value =>
            {
                switch(transition)
                {
                    case Transition.ColorTint: image.color = isOn ? colors.pressedColor : colors.normalColor; break;
                    case Transition.SpriteSwap: image.sprite = isOn ? spriteState.pressedSprite : normalSprite; break;
                    default: throw new NotImplementedException();
                }
            });
	}

}
