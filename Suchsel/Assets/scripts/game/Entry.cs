// filename: Entry.cs

/// <summary>
/// This class is a datastructure for the words in the gird.
/// </summary>
public class Entry
{
    /// <summary>
    /// A word can be aligned horizontal or vertical
    /// </summary>
    public enum AlignmentType
    {
        horizontal,
        vertical
    }

    /// <summary>
    /// The direction in which a word is written can be normal or reverse
    /// </summary>
    public enum DirectionType
    {
        normal,
        reverse
    }

    /// <summary>
    /// returns the first letter of the word.
    /// </summary>
    public char Letter{
        get
        {
            return Word[0];
        }
    }

    /// <summary>
    /// the complete word.
    /// </summary>
    public string Word
    {
        get;
        set;
    }

    /// <summary>
    /// Getter/Setter for the Alignment
    /// </summary>
    public AlignmentType Alignment
    {
        get;
        set;
    }

    /// <summary>
    /// Getter/Setter for the Direction
    /// </summary>
    public DirectionType Direction
    {
        get;
        set;
    }

    /// <summary>
    /// Getter/Setter for the colum in which the word begeins.
    /// </summary>
    public int Col
    { set; get; }

    /// <summary>
    /// Getter/Setter for the row in which the word begeins.
    /// </summary>
    public int Row
    { set; get; }

    /// <summary>
    /// Default Constructor
    /// </summary>
    public Entry()
    {
        Word = "";
        Alignment = AlignmentType.vertical;
        Direction = DirectionType.normal;
        Col = 0;
        Row = 0;
    }

    /// <summary>
    /// Overloaded Constructor
    /// </summary>
    public Entry(string word, Entry.AlignmentType alignment, int col, int row, Entry.DirectionType direction = DirectionType.normal)
    {
        Word = word;
        this.Alignment = alignment;
        Col = col;
        Row = row;
        this.Direction = direction;
    }

    /// <summary>
    /// Overwritten ToString
    /// </summary>
    /// <returns></returns>
    new public string ToString()
    {
        return Word + ", " + Alignment.ToString() + ", " + Col + ", " + Row + ", " + Direction.ToString();
    }
        
}
