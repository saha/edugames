// filename: GridSlot.cs
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

/// <summary>
/// This script controlls ONE Slot in the Grid.
/// </summary>
    public class GridSlot : MonoBehaviour
    {
        // the X position in th grid
        [HideInInspector]
        public int XPos { set; get; }

        // the Y position in the grid
        [HideInInspector]
        public int YPos { set; get; }

        private List<string> words;

        // each slot knows the words it is part of.
        public List<string> PartOfWords;

        // sometimes words are overlapping. In that case this slot needs to be counted multiple times.
        public int NecessaryCounts { get { return PartOfWords.Count; } } 

        // to remember the times this slot was allready counted.
        public int TimesCounted {get; set;} 

        // to mark the slot as selected
        [HideInInspector]
        public bool Selected { get; set; }

        // the content of the slot.
        public string Letter { get { return transform.GetChild(0).GetComponent<Text>().text; } }

        // the Unity.UI Text component of the child of this slot.
        public Text Text { get { return transform.GetChild(0).GetComponent<Text>(); } }

    }
