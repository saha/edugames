// filename: StateAfterRound.cs
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// This state in entered after the player found every hidden word.
/// At the moment there is nothing implemented that happens after a level is cleared.
/// 
/// Things that could be implemented here are: A UI prompt and/or some fancy animations.
/// </summary>
public class StateAfterRound : GameState
{

    /// <summary>
    /// Init
    /// </summary>
    private void Start() { }

    /// <summary>
    /// Implementation of the abstract OnStateEntered method
    /// </summary>
    public override void OnStateEntered()
    {
        // Nothing to do in this application.
        // Just enter a new game mode.
        gameManager.NewGameState(gameManager.statePlaying);
    }

    /// <summary>
    /// Implementation of the abstract OnStateQuit method
    /// </sum
    public override void OnStateQuit()
    {
       
    }

    /// <summary>
    /// Implementation of the abstract StateUpdate method
    /// </summary>
    public override void StateUpdate()
    {
      
    }
}
