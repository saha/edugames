// filename: StatePlaying.cs
using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;


/// <summary>
/// This state controlls the game play of the application.
/// This script calls the functions that create the game field and loads the level.
/// </summary>
public class StatePlaying : GameState
{
    // the area where the words are placed
    public Slots wordArea;

    // every word from every level.
    public static List<List<string>> levels;

    /// <summary>
    /// Init.
    /// </summary>
	private void Start ()
    {
        // get the component from the GameObject and save it.
        wordArea = wordArea.GetComponent<Slots>();

        // create a list of lists.
        levels = new List<List<string>>();
	}

    /// <summary>
    /// Implementation of the OnStateEntered Method.
    /// </summary>
    public override void OnStateEntered()
    {
        // start the level.
        StartLevel(GameData.Level);
    }

    /// <summary>
    /// Implementation of the OnStateQuit Method.
    /// </summary>
    public override void OnStateQuit() 
    { 
        // clean up.
        wordArea.Clear();
    }

    /// <summary>
    /// Implementation of the StateUpdate Method.
    /// </summary>
    public override void StateUpdate() 
    {
        // check for key input
        if (Input.GetKeyDown(KeyCode.Escape))
            gameManager.NewGameState(gameManager.stateInput);
    }

    /// <summary>
    /// Public access point to start the next level.
    /// </summary>
    public void NextLevel()
    {
        gameManager.NewGameState(gameManager.statePlaying);
    }

    /// <summary>
    /// Start level <para>i</para>.
    /// </summary>
    /// <param name="i"></param>
    private void StartLevel(int i)
    {       
        // check if the highest level is reached.
        // if true set the loaded level to max.
        if (i > levels.Count) i = levels.Count - 1;

        // set the necessary amount of letters which the player needs to fill in correctly. 
        WinconditionHandler.LettersToPlace = levels[i].Count;
        // insert the words into the UI.
        wordArea.InsertWords(levels[i]);
    }
}