// filename: DragHandler.cs
using UnityEngine;
using UnityEngine.EventSystems;

/// <summary>
/// Attach this script to every GameObject you want to drag around.
/// This is an extented version of the Draghandler from the Drag&Drop package.
/// </summary>
    public class DragHandler : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler
    {
        // The item that is beeing draged.
        public static GameObject itemBeingDraged;
        // The start position of the item.
        public static Vector3 startPosition;
        // the start parent of the item.
        public static Transform startParent;

        // This is used to lock a completed word.
        public bool Finished { get; set; }

        // a empty game object that is used to keep the item that is being dragged on top of everything else.
        private Transform dragFix;

        /// <summary>
        /// Init.
        /// </summary>
        private void Start()
        {
            // get the transform component of the dragfix and save it.
            dragFix = GameObject.Find("LetterArea").transform;
        }

        /// <summary>
        /// IBeginDragHandler Implementation.
        /// This is called as soon as you begin to drag.
        /// </summary>
        /// <param name="eventData"></param>
        public void OnBeginDrag(PointerEventData eventData)
        {
            // set static variables
            itemBeingDraged = gameObject;
            startPosition = transform.position;
            startParent = transform.parent;

            // set the parent to dragfix
            transform.SetParent(dragFix);

            // Don't forget this.
            GetComponent<CanvasGroup>().blocksRaycasts = false;
        }

        /// <summary>
        /// IDragHandler
        /// This is called as long as you drag.
        /// </summary>
        /// <param name="eventData"></param>
        public void OnDrag(PointerEventData eventData)
        {
            transform.position = Input.mousePosition;
        }

        /// <summary>
        /// Is always called last. Which means after OnDrop and after IHaschanged.
        /// </summary>
        /// <param name="eventData"></param>
        public void OnEndDrag(PointerEventData eventData)
        {
            itemBeingDraged = null;
            Debug.Log("Drag End");

            // only unlock it if the word is not finished yet.
            if(!Finished)
            GetComponent<CanvasGroup>().blocksRaycasts = true;
        }
    }
