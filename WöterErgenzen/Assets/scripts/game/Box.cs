using UnityEngine.UI;
using System.Collections.Generic;

/// <summary>
/// A Simple Class to simplify the grouping of Slots,
/// Now: List<Box> Before: List<List<Box>>
/// </summary>
public class Box  {

    // A Unity GameObject serves as the parent.
    public UnityEngine.GameObject _wordBox { get; set; }
  
    // returns number of slots in this box.
    public int SlotCount { get { return _slots.Count; } }

    // a list of the slots
    private List<Slot> _slots;

    /// <summary>
    /// Getter for _slots
    /// </summary>
    public List<Slot> Slots { get { return _slots; } }

    // each box has an ID to identify it easier later on.
    private static int ID = 0;

    // this is the place in the hierachy. index 0 would be the first child of the box index 1 the second child ...
    private int _index;

    /// <summary>
    /// Getter for _index.
    /// </summary>
    public int Index { get { return _index;} }

    /// <summary>
    /// Getter - Returns the Content of the box.
    /// This function  goes through each slot in the box, gets its content(a content), builds a string out of each content and returns it.
    /// </summary>
    public string Content
    {
        get
        {
            string s = "";
            // go through each slot 
            foreach(var slot in _slots)
            {
                // get the component
                Text t = slot.content.GetComponent<Text>();
                if (t) // if there is a component add the content to out return string
                    s += t.text[0];
                else // there was no content in this slot
                    s += " "; // add empty sign to the return string.
            }
            return s;
        }
    }

    /// <summary>
    /// Clears the slot list and resets the ID.
    /// </summary>
    public void Clear()
    {
        _slots.Clear();
        ID = 0;
    }

    /// <summary>
    /// Returns the slot at index <para>i</para>.
    /// </summary>
    /// <param name="i"></param>
    /// <returns></returns>
    public Slot Slot(int i)
    { return _slots[i]; }

    /// <summary>
    /// Adds <para>slot</para> to the list.
    /// </summary>
    /// <param name="slot"></param>
    public void AddSlot(Slot slot)
    {
        _slots.Add(slot);
    }

    /// <summary>
    /// Contructor.
    /// </summary>
    /// <param name="wordBox"></param>
    public Box(UnityEngine.GameObject wordBox)
    {
        _wordBox = wordBox;
        _slots = new List<Slot>();
        _index = ID++;
    }
    
}
