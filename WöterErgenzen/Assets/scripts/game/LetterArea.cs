// filename: LetterArea.cs
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections.Generic;

/// <summary>
/// In the application exists an area where the letters, which are needed to be filled into the words, are placed.
/// This script managed that area.
/// </summary>
public class LetterArea : MonoBehaviour, IDropHandler
{
	/// <summary>
	/// Implementation of the IDropHandler interface.
	/// </summary>
	/// <param name="eventData"></param>
	public void OnDrop (PointerEventData eventData)
	{
		Debug.Log ("Area OnDrop: " + eventData.position);
		// set the parent of the item that was droped.
		DragHandler.itemBeingDraged.transform.SetParent (transform);
	}

}
