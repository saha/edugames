// filename: WinconditionHandler.cs
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections.Generic;

/// <summary>
/// This script implements the IHasChanged interface and checks if the player has a word completed and if the level is cleared.
/// This script is attached to the WordBox prefab.
/// This script is triggerd by drop events.
/// </summary>
public class WinconditionHandler : MonoBehaviour, IHasChanged
{
    // The number of letters that need to be placed in order the clear the level.
    public static int LettersToPlace;
    // The numver of letter that are currently palced.
    public static int LettersPlaced = 0;

    // A reference to the playing state.
    private StatePlaying statePlaying;

    // UI element that displays the points.
    private Text pointLabel;
    // UI element that displays the level.
    private Text levelLabel;
    
    /// <summary>
    /// Init.
    /// </summary>
    private void Start()
    {
        // Find the gameobjects, get the components and save them.
        statePlaying = GameObject.Find("GameManager").GetComponent<StatePlaying>();
        pointLabel = GameObject.Find("Text_Punkte").GetComponent<Text>();
        levelLabel = GameObject.Find("Text_Level").GetComponent<Text>();
    }

    /// <summary>
    /// Implementation of the IHasChanged interface.
    /// </summary>
    void IHasChanged.HasChanged()
    {
        Debug.Log("Wincondition Handler IHasChanged\n " + Content + " - " + Slots.endResult[Index] + " Index: " + Index);

        // check if the content of the box is correct
        if(Content == Slots.endResult[Index]) // word is correct.
        {
            // lock the box
            Lock();
            // increment the found letters.
            LettersPlaced++;
            // update UI.
            GameData.UpdatePointLable(pointLabel, true);
        }

        // check if the player placed all letters correctly
        if( LettersPlaced == LettersToPlace) 
        {
            // update AND increment the level/UI
            GameData.UpdateLevelLable(levelLabel, true);
            // reset the letters placed counter.
            LettersPlaced = 0;
            // start the next level.
            statePlaying.NextLevel();
        }
    }

    /// <summary>
    /// Getter - Returns the hierachy index the box. 
    /// </summary>
    int Index
    {
        get
        {
            // get the parent.
            Transform parent = transform.parent;
            for(int i=0;i<parent.childCount;i++)
            {
                // find THIS gameobject in the children.
                if(parent.GetChild(i).gameObject == this.gameObject)
                {
                    return i;
                }
            }
            // not found
            return -1;
        }
    }

    /// <summary>
    /// Getter - Returns the returns the content of the box as string.
    /// </summary>
    string Content
    {
        get
        {
            string s = "";
            foreach(var child in transform.GetComponentsInChildren<Slot>())
            {
                GameObject letter = child.content;
                if (letter)
                {
                    Text t = letter.GetComponent<Text>();
                    if (t)
                        s += t.text; 
                }
                else
                    s += " ";
            }
            return s;
        }
    }

    /// <summary>
    /// Locks the content of the box.
    /// </summary>
    private void Lock()
    {
        Debug.Log("Locking Box " + Index);
        // go through every slot of the box
        for (int i = 0; i < transform.childCount; i++)
        {
            GameObject child = transform.GetChild(i).gameObject;     
            // highlight the slot
            child.GetComponent<Slot>().content.GetComponent<Text>().color = Utils.UnityGreen;

            // mark it as finished.
            child.GetComponent<Slot>().content.GetComponent<DragHandler>().Finished = true;

            // lock the slot.
            child.GetComponent<Slot>().content.GetComponent<CanvasGroup>().blocksRaycasts = false;
        }
    }
}

// Define new Event Interface.
namespace UnityEngine.EventSystems
{
    public interface IHasChanged : IEventSystemHandler
    {
        void HasChanged();
    }
}