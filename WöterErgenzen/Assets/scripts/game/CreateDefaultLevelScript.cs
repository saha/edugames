// filename: CreateDefaultLevelsScript.cs
using UnityEngine;
using System.Collections;
using System.IO;
using System.Linq;


/// <summary>
/// A script to create level files from resources after the built.
/// </summary>
    public class CreateDefaultLevelScript : MonoBehaviour
    {
        // the number of default level files.
        private int NumberOfDefaultLevels;

        /// <summary>
        /// Init.
        /// </summary>
        private void Start()
        {
            // This part was used for debugging and testing within the UnityEditor.
            
            // Check if UnityEditor
            if (Application.platform == RuntimePlatform.WindowsEditor)
            {
                // Get all non .meta files and orderthem by name in a array.
                DirectoryInfo dir = new DirectoryInfo(LevelConfig.Levels_Directoy);
                FileInfo[] files = dir.GetFiles()
                   .Where(val => !val.Name.Contains(".meta"))
                   .OrderBy(p => p.Name)
                   .ToArray();
                NumberOfDefaultLevels = files.Length;
            Debug.Log(NumberOfDefaultLevels);
            }

            // This is the important part for the built application.
            // call the function that creates the level files.
            CreateDefaultLevelFiles();
        }

        /// <summary>
        /// Reads Level files from resources and saves them into a txt files withinn a subdir of the Application data folder. 
        /// </summary>
        private void CreateDefaultLevelFiles()
        {
            // Check if level directory exitsts.
            if (!Directory.Exists(LevelConfig.Levels_Directoy))
                Directory.CreateDirectory(LevelConfig.Levels_Directoy);

            // get every file from the level subdir as textasset.
            var tas = Resources.LoadAll<TextAsset>("Levels/");
            for (int i = 0; i < tas.Length; i++)
            {
                // the path to the directory the files will be placed in.
                string file = LevelConfig.Path_And_Filename + "_" + i.ToString() + ".txt";
                // check if the file allready exists..
                if (!File.Exists(file)) // if not.
                    File.AppendAllText(file, tas[i].text);
            }
     
        }
    }

