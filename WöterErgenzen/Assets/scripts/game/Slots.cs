// filename: Slots.cs
using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

public class Slots : MonoBehaviour
{

	// the boxes in which the words are placed in.
	[HideInInspector]
	public static List<Box>
		boxes;

	// the letters that are removed from the words.
	[HideInInspector]
	public static string
		vocals = "aeiou";

	// a list to remember how the words should look like.
	[HideInInspector]
	public static List<string>
		endResult;

	// reference to the slotPrefab
	public GameObject slotPrefab;

	// reference to the boxPrefab
	public GameObject boxPrefab;

	// reference to the letterPrefab
	public Text letterPrefab;

	// the area in wich the 'vocals' will be dropped in
	public LetterArea letterArea;

	/// <summary>
	/// Init.
	/// </summary>
	void Start ()
	{
		// get the LetterArea component from the GameObject and save it.
		letterArea = letterArea.GetComponent<LetterArea> ();

		// create the lists.
		boxes = new List<Box> ();
		endResult = new List<string> ();
	}

	/// <summary>
	/// This function makes a box and puts <para>cnt</para>> slots into it.
	/// </summary>
	/// <param name="cnt"></param>
	/// <returns>The index of the box in which the slots were filled into.</returns>
	private int MakeSlots (int cnt)
	{
		// Create a wordBox for the slots
		GameObject wordBox = Instantiate (boxPrefab);
		wordBox.transform.SetParent (transform, false);
		wordBox.name = "box";

		Box box = new Box (wordBox);

		// Fill the wordBox with slots
		for (int i = 0; i < cnt; i++) {
			GameObject obj = Instantiate (slotPrefab);
			obj.name = "Slot " + i;
			Slot s = obj.GetComponent<Slot> ();
			if (s) {
				s.transform.SetParent (wordBox.transform, false);
				s.index = i;
				box.AddSlot (s);
			} else {
				Debug.LogError ("Error while retriving <Slot> Component out of instantiated prefab. GetComponent Returned null.");
				return -1;
			}
		}
		boxes.Add (box);
		Debug.Log ("Made a Box\n Length: " + box.SlotCount + ", Index: " + box.Index);
		return box.Index;
	}

	/// <summary>
	/// Clear the area from boxes. Used after a round is finished. Called from StatePlaying OnStateQuit Function.
	/// </summary>
	public void Clear ()
	{
		for (int i = 0; i < transform.childCount; i++) {
			Destroy (transform.GetChild (i).gameObject);
		}
		foreach (var box in boxes)
			box.Clear ();

		boxes.Clear ();
		endResult.Clear ();
	}

	/// <summary>
	/// Method for other scripts to access.
	/// </summary>
	/// <param name="levels">A List of levels</param>
	public void InsertWords (List<string> words)
	{
		foreach (var word in words) {
			InsertWord (word);
			Slots.endResult.Add (word);
		}
	}

	/// <summary>
	/// Puts a character into a special Slot prefab and places it in a random position onto the drop area.
	/// </summary>
	/// <param name="c">The character in the Slot.</param>
	private void ShuffleIntoDropArea (char c)
	{
		Text t = Instantiate (letterPrefab);
		t.name = c.ToString ();
		if (t) {
			t.transform.SetParent (letterArea.transform, true);
			t.text = c.ToString ();

			// clac new position
			RectTransform rt = letterArea.GetComponent<RectTransform> ();
			float x = rt.position.x + Random.Range (rt.rect.xMin + 2 * t.rectTransform.rect.width, rt.rect.xMax - 1.5f * t.rectTransform.rect.width);
			float y = rt.position.y + Random.Range (rt.rect.yMin + 2 * t.rectTransform.rect.height, rt.rect.yMax - 1.5f * t.rectTransform.rect.height);

			t.transform.position = new Vector2 (x, y);
		}
	}

	/// <summary>
	/// Crates a Slot for evert character in the word, looks for every vocal and puts them into a different area.
	/// </summary>
	/// <param name="word">The word</param>
	private void InsertWord (string word)
	{
		// some error handling
		if (word == null) {
			Debug.LogError ("Given Parameter was null.");
			return;
		}
		if (word.Length <= 0) {
			Debug.LogError ("Length of given paramter was 0 or negative");
			return;
		}

		// create the slots for the word
		int boxIndex = MakeSlots (word.Length);

		Debug.Log ("Filling word: " + word + " into box " + boxIndex);
		for (int i = 0; i<word.Length; i++) {
			// check if the word has any vocals in it. (aeiou)
			if (word [i].ToString ().ToLower ().IndexOfAny (vocals.ToCharArray ()) != -1) {
				// char at i is aeiuo.
				// Throw it into the drop
				ShuffleIntoDropArea (word [i]);
				Destroy (boxes [boxIndex].Slot (i).content);
			} else if (word [i].ToString ().Equals (" ")) {
				// char at i is " " (space)
				// make the Slot invisble and remove ondrop trigger.
				boxes [boxIndex].Slot (i).gameObject.GetComponent<Image> ().enabled = false;
				boxes [boxIndex].Slot (i).gameObject.GetComponent<Slot> ().enabled = false;
				boxes [boxIndex].Slot (i).gameObject.GetComponent<Slot> ().content.GetComponent<Text> ().text = " ";
			} else {
				// char at i is a non vocal and non space,
				// Put char at i in a Slot of a box.
				boxes [boxIndex].Slot (i).content.GetComponent<Text> ().text = word [i].ToString ();
				boxes [boxIndex].Slot (i).content.name = word [i].ToString ();
				// lock the non vocals .
				boxes [boxIndex].Slot (i).transform.GetComponentInChildren<DragHandler>().enabled = false; 
			}
		}

	}
}
