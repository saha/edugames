// filename: GameManager.cs
using UnityEngine;

/// <summary>
/// This Class manages the state of the game.
/// Taken from Smith's and Queiroz's Unity 4.x cookbook, p. 303.
/// </summary>
    public class GameManager : MonoBehaviour
    {
        // STATES - put here every state that your game has

        public StateInput stateInput;
        public StatePlaying statePlaying;
        public StateAfterRound stateAfterRound;

        // States - end

        // the current state of the game
        private GameState currentState;

        /// <summary>
        /// Init - Before Start
        /// </summary>
        private void Awake()
        {
            // Get the Componets from the GameObjects and save them.
            stateInput = GetComponent<StateInput>();
            statePlaying = GetComponent<StatePlaying>();
            stateAfterRound = GetComponent<StateAfterRound>();
        }

        /// <summary>
        /// Init - After Awake
        /// </summary>
        private void Start()
        {
            // enter the inputstate
            NewGameState(stateInput);
        }

        /// <summary>
        /// Calls the Update function of the <c>currentState</c>.
        /// </summary>
        private void Update()
        {
            if (currentState != null)
                currentState.StateUpdate();
        }

        /// <summary>
        /// Use this Function to enter a new state.
        /// 
        /// gameManager.NewGameState(gameManager.<GameState>);
        /// </summary>
        /// <param name="newState"></param>
        public void NewGameState(GameState newState)
        {
            Debug.Log("Entering new gamestate: " + newState);
            if (null != currentState)
                currentState.OnStateQuit();

            currentState = newState;
            currentState.OnStateEntered();
        }

    }
