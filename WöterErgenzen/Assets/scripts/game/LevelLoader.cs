// file: LevelLoader.cs

// This is necessary for web builds.
// Web builds don't support all Systen.IO methods like Windows. Also the Paths are different.
#if !UNITY_WEBPLAYER
#define SAVE_OK
#endif

    using System.Collections.Generic;
    using System.IO;
    using System.Linq; // So usefull!
    using UnityEngine;

/// <summary>
/// This class is loads the levels.
/// It is attached to the LevelLoader GameObject, which is a child of the GameManager GameObject.
/// </summary>
    public class LevelLoader : MonoBehaviour
    {
        // define the maximum lenght of a word.
        public static int MaxWordLenght = 20;

        /// <summary>
        /// Init
        /// </summary>
        private void Start()
        {
            LoadLevels();
        }

        /// <summary>
        /// This function loads every level that is available.
        /// </summary>
        public void LoadLevels()
        {
#if SAVE_OK
            Debug.Log("Loading words...");

            // the level that is loaded.
            int lvl = 0;

            // the number of words in that level.
            int wordCount = 0;

            // Get all txt files from the level directory
            DirectoryInfo dir = new DirectoryInfo(LevelConfig.Levels_Directoy);

            // Sort all Levels by their name and remove all files containung .meta in it.
            // Order is descending ( 0 ist first 1 is second 2 third and so on).
            FileInfo[] files = dir.GetFiles()
                .Where(val => !val.Name.Contains(".meta"))
                .OrderBy(p => p.Name)
                .ToArray();

            // go through every found file.
            foreach (var file in files)
            {
                // get the content of the file
                string content = File.ReadAllText(file.FullName);

                // create temp list
                List<string> temp = new List<string>();

                // go through every word with in the file.
                foreach (string word in content.Split(','))
                {
                    // remove SPACES
                    string tempWord = word.Trim();
                    // check if the word exceeds the maximum.
                    if (tempWord.Length > MaxWordLenght)
                    {
                        Debug.Log("to long: " + word + ", " + tempWord.Length + "\n Capping the word to " + MaxWordLenght + " character.");
                        // just cut the rest off.
                        tempWord = tempWord.Substring(0, MaxWordLenght);
                    }
                    // add the word to the temp list.
                    temp.Add(tempWord);
                    // increment the word count.
                    wordCount++;
                }
                // add the temp list to the list of lists.
                StatePlaying.levels.Add(temp);
                // increament the level count.
                lvl++;
            }
            Debug.Log("Loaded " + wordCount + " words in " + lvl + " levels.");
#endif
        }
    }