// filename: Slot.cs
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

/// <summary>
/// This script enables the GameObject it is attached to to receive Drop Events.
/// Attach this script to every GameObject you want to drop other GameObjects onto. 
/// This Script works with DropHandler Scripts.
/// 
/// <remarks>This is version of Slot.cs ia an modified Version of the Slot.cs from the Drag&Drop package</remarks>
/// </summary>
public class Slot : MonoBehaviour, IDropHandler, IPointerEnterHandler, IPointerExitHandler
{
    /// <summary>
    /// Used if this slots has silblings.
    /// </summary>
    public int index
    { get; set; }

    /// <summary>
    /// Returns the first child of the slot. Which is the thing we are going to drag into another slot.
    /// </summary>
    public GameObject content
    {
        get
        {
            if (transform.childCount > 0)
                return transform.GetChild(0).gameObject;
            return null;
        }
    }

    /// <summary>
    /// In this application some slots are palced into a box.
    /// This method returns the box the slot is placed into.
    /// </summary>
    public GameObject box
    {
        get
        {
            if (transform.parent.gameObject.name == "box")
                return transform.parent.gameObject;
            return null;
        }
    }

    /// <summary>
    /// Implemntation of the IDropHandlerInterface
    /// </summary>
    /// <param name="eventData"></param>
    public void OnDrop(PointerEventData eventData)
    {
        // only do this if there is an item being dragged
        if (DragHandler.itemBeingDraged)
        {
            Debug.Log("Slot Drop");
            // revert the color of the item
            GetComponent<Image>().color = Utils.White;
            // set teh parent of the item
            DragHandler.itemBeingDraged.transform.SetParent(transform);
            // set the postion of the item.
            DragHandler.itemBeingDraged.transform.position = transform.position;

            // Don't forget this.
            ExecuteEvents.ExecuteHierarchy<IHasChanged>(gameObject, null, (x, y) => x.HasChanged());
        }
    }

    /// <summary>
    /// Implemntation of the IPointerEnterHandler
    /// Highlight a slot below the cursor while dragging.
    /// </summary>
    /// <param name="eventData"></param>
    public void OnPointerEnter(PointerEventData eventData)
    {
        if(DragHandler.itemBeingDraged)
        {
            GetComponent<Image>().color = Utils.UnityGreen;
        }
    }

    /// <summary>
    /// IPointerExitHandler
    /// Reverts the color of the slot after hovering over it while dragging.
    /// </summary>
    /// <param name="eventData"></param>
    public void OnPointerExit(PointerEventData eventData)
    {
        if (DragHandler.itemBeingDraged)
        {
            GetComponent<Image>().color = Utils.White;
        }
    }
}