// filename: LevelConfig.cs
using UnityEngine;
using System.IO;
using System.Collections.Generic;

/// <summary>
/// A static class containing all relevant information about a level.
/// </summary>
public static class LevelConfig
{
    // the name of the file.
    // This is an extra variable, because Unity Editor and built Unity Applications use a differnt Path system.
    // Example: 
    // In Editor: Application.dataPath = Assets Folder
    // Built Windows Player:  Application.dataPath = ProjectName_data
    // Also Resources.Load() doesn't want file extentions. 
    public static string Filename = "Level";

    // Path to the directory containing the level files.
    public static string Levels_Directoy = Application.dataPath + Path.DirectorySeparatorChar + "Resources" + Path.DirectorySeparatorChar + "Levels" + Path.DirectorySeparatorChar; // windows_Data/Resources/Levels/
    public static string Path_And_Filename = Levels_Directoy + Filename;  // windows_Data/Resources/Levels/Level

    // Difficulty Seeting. Sets all letters to small.
    public static bool OnlySmallLetters;

    // number of correclty placed letters.
    public static int correctLetters;

    // total number of the letters which need to be placed correctly.
    // Letter count of every word in the level.
    public static int neededLetters;
}
