// filename: InputCaretFix.cs
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

/// <summary>
/// THis is a workaround for a bug that causes to not properply display the selecetion highlighting for Input Caret.
/// Attatch this script to every Input Field.
/// <remarks>
/// No Bug was reported on the Unity Issue Tracker. This Bug first appeared in version 5.2.0f3
/// </remarks>
/// 
/// </summary>
public class InputCaretFix : MonoBehaviour, ISelectHandler
{
    private bool imageAdded;

    /// <summary>
    /// Implementation of the ISelectHandler interface.
    /// </summary>
    /// <param name="eventData"></param>
    public void OnSelect(BaseEventData eventData)
    {

    	if (imageAdded) return;
        // Since the Carets are only temporary created during runtime, we need to Find them during runtime.
        RectTransform caret = (RectTransform)transform.Find(gameObject.name + " Input Caret");
        if (caret == null) return;
      
       	// Get a reference to the Image Component.
        Image i = caret.gameObject.GetComponent<Image>();
        // check if the component is not null
        if (!i)
        {
            caret.gameObject.AddComponent<Image>();
            imageAdded = true;
            Debug.Log("CaretFix added image");
        }    
    }
}