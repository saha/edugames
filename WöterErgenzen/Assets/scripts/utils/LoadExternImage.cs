// filename LoadExternImage.cs
using UnityEngine;
using UnityEngine.UI;
using System.IO;
using System.Collections;

/// <summary>
/// Loads via the WWW class an image from PicturePath variable, converts it to a sprite and sets it to a Unity.UI.Image Component.
/// Use this script for Images that the User might want to change.
/// </summary>
public class LoadExternImage : MonoBehaviour
{
    public string PicturePath;

    // Use this for initialization
    IEnumerator Start()
    {
        if (File.Exists(PicturePath))
        {
            string url = "file:///" + PicturePath;
            WWW www = new WWW(url);
            yield return www;

            if (www.isDone)
            {
                if (www.texture != null)
                {
                    var tex = www.texture;
                    Image image = GetComponent<Image>();
                    image.sprite = Sprite.Create(tex, new Rect(0,
                                                               0, 
                                                               tex.width, 
                                                               tex.height),
                                                      new Vector2(0.5f, 0.5f));
                }
            }
        }
        else
        {
            gameObject.SetActive(false);
        }

       
    }
}

