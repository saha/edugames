using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Reads the static varialbe GameConfig.Application_Version and adds it in a Text object.
/// </summary>
public class SetVersion : MonoBehaviour {

	// Use this for initialization
	void Start () {
        // get the version string
        string version = GameConfig.Application_Version;
        
        // get our Text object
        Text text = gameObject.GetComponent<Text>(); 

        // add the text to Text.text
        text.text += version;
	}
	
}
